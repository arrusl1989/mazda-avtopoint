<?php


namespace App;


abstract class Controller
{

  protected $view;

  protected $version;

  public function __construct()
  {
    $this->view = new \App\Views\View();
    $this->version = Config::version;
  }

  abstract function __invoke();

}

<?php


namespace App\Tools;

/*
 * Класс маршрутизации
*/
class Router
{
  /**
   * URI
   *
   * @var string
   */
  private $uri;

  /**
   * Путь до директории с контроллерами
   *
   * @var string
   */
  private $pathToPublicControllers = ROOT_PATH . '/../app/Controllers/';

  /**
   * Namespace контроллеров
   *
   * @var string
   */
  private $namespaceController = 'App\Controllers\\';

  /**
   * Контроллер по умолчанию
   *
   * @var string
   */
  private $defaultController = 'App\Controllers\Index';

  /**
   * Получаем имя класса из запроса
   *
   * @return string
   */
  public function getClass() {

    $parts = explode('/', $_SERVER['REQUEST_URI']);
    $route = $parts[1];
    $className = ucfirst($route);

    return $this->classExists($route, $className)
      ? $this->namespaceController . $className
      : $this->defaultController;
  }

  /**
   * Существует ли контроллер для запрашиваемого маршрута
   *
   * @param $route
   * @param $className
   * @return bool
   */
  private function classExists(string $route, string $className) {
    return $route && file_exists($this->pathToPublicControllers . $className . '.php');
  }

}

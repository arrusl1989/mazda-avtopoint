<?php


namespace App\Controllers;

/*
 * Класс маршрута по умолчанию
 *
 * Если обращение происходит к контроллеру, который не описан в папке API,
 * будет вызван этот контроллер
*/

class policy extends \App\Controller
{

  public function __invoke()
  {

    $title = 'Политика безопасности';
    $version = '?v=' . $this->version;

    $this->view->display(__DIR__ . '/../../views/security-policy.php');

  }

}

<?php


namespace App\Controllers;


use App\Form\Factory;
use App\Form\Uploader;
use App\Form\Form;

/*
 * Класс для отправки форм
*/
class Send
{

  public function __invoke()
  {

    $formController = new Factory();

    $action = filter_input(INPUT_POST, 'action');

    $pt = filter_input_array(INPUT_POST, $formController->filterArray($action));
    $result = $formController->processing($pt);

    $uploadFiles = new Uploader();
    $images = $uploadFiles->getPathToFiles();

    if ($result['status'] === 'success') {
      $form = new Form($result['data']['action']);

      $send = $form->sendLetter($result['data'], $images);

      $uploadFiles->deleteMovedFiles();

      if ($send === true) {
        $result = [
          'status' => 'success',
          'content' => '<div class="thanks-form"><p>Спасибо! Ваша заявка успешно отправлена.</p><p>Наши менеджеры свяжутся с Вами в ближайшее время.</p></div>',
          'data' => $result['data'],
        ];
      } else {
        $result = [
          'status' => 'error',
          'content' => '<div class="thanks-form"><p>Ошибка! Попробуйте позже</p></div>'
        ];
      }
    }

    echo json_encode($result);

  }

}

<?php


namespace App\Controllers;

/*
 * Класс маршрута по умолчанию
 *
 * Если обращение происходит к контроллеру, который не описан в папке API,
 * будет вызван этот контроллер
*/

class Index extends \App\Controller
{

  public function __invoke()
  {

    $title = 'Mazda Автопойнт';
    $version = '?v=' . $this->version;

    include __DIR__ . '/../../views/chunk/header.php';

    $this->view->display(__DIR__ . '/../../views/index.php');

    include __DIR__ . '/../../views/chunk/footer.php';

  }

}

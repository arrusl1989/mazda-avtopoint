<?php


namespace App\Views;


class View
{

  public function render($template)
  {
    ob_start();
    include $template;
    $content = ob_get_contents();
    ob_end_clean();
    return $content;
  }

  public function display($template)
  {
    include $template;
  }


}

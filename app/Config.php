<?php

namespace App;

/*
 * Класс с конфигурационными данными
*/
class Config
{

  /**
   * Версия для стилей и скриптов
   * @var string
   */
  const version = '0.0.10';

  /**
   * Имя контакта в отправляемых письмах
   * @var string
   */
  protected $mail_name = 'Заявка с лендинга service-avtopoint-mazda.ru';

  /**
   * Почта с которой будет производиться отправки сообщений
   * @var string
   */
  protected $mail_from = 'service-avtopoint-mazda@4px.tech';

  /**
   * Пароль для отправки почты
   * @var string
   */
  protected $mail_password = 'Y6mDjNVdL6HN';

  /**
   * Список форм с полями
   * @var array
   */
  protected $form_list = [
    'feedback' => [
      'title' => 'Заказать звонок',
      'field' => [
        'name' => [
          'label' => 'Имя',
          'required' => 0,
          'format' => 'string',
        ],
        'phone' => [
          'label' => 'Телефон',
          'required' => 1,
          'format' => 'string',
        ],
      ]
    ],
    'service' => [
      'title' => 'Записаться на сервис',
      'field' => [
        'name' => [
          'label' => 'Имя',
          'required' => 0,
          'format' => 'string',
        ],
        'phone' => [
          'label' => 'Телефон',
          'required' => 1,
          'format' => 'string',
        ],
      ]
    ],
    'service_order' => [
      'title' => 'Записаться на сервис',
      'field' => [
        'name' => [
          'label' => 'Имя',
          'required' => 0,
          'format' => 'string',
        ],
        'phone' => [
          'label' => 'Телефон',
          'required' => 1,
          'format' => 'string',
        ],
        'services-list' => [
          'label' => 'Список требуемых услуг',
          'required' => 1,
          'format' => 'string',
        ],
        'services-price' => [
          'label' => 'Сумма',
          'required' => 1,
          'format' => 'string',
        ],
      ]
    ],
    'master' => [
      'title' => 'Запись к мастеру',
      'field' => [
        'name' => [
          'label' => 'Имя',
          'required' => 0,
          'format' => 'string',
        ],
        'phone' => [
          'label' => 'Телефон',
          'required' => 1,
          'format' => 'string',
        ],
        'master' => [
          'label' => 'Мастер',
          'required' => 1,
          'format' => 'string',
        ],
      ]
    ],
    'luck' => [
      'title' => 'Испытайте удачу',
      'field' => [
        'name' => [
          'label' => 'Имя',
          'required' => 0,
          'format' => 'string',
        ],
        'phone' => [
          'label' => 'Телефон',
          'required' => 1,
          'format' => 'string',
        ],
        'gift' => [
          'label' => 'Подарок',
          'required' => 1,
          'format' => 'string',
        ],
      ]
    ],
    'assesment' => [
      'title' => 'Узнайте стоимость ремонта по фото',
      'field' => [
        'name' => [
          'label' => 'Имя',
          'required' => 0,
          'format' => 'string',
        ],
        'phone' => [
          'label' => 'Телефон',
          'required' => 1,
          'format' => 'string',
        ],
      ]
    ],
  ];

  /**
   * Получатели заявок с форм
   * @var array
   */
  protected $form_contacts = [
    'default' => 'okrylova@alarm-motors.ru, abenedishchuk@alarm-motors.ru, vdergletckaia@alarm-motors.ru, online@alarm-motors.ru, dkupris@alarm-motors.ru',
    'assesment' => 'ruslan@minnibaev.ru, okrylova@alarm-motors.ru, abenedishchuk@alarm-motors.ru, vdergletckaia@alarm-motors.ru, online@alarm-motors.ru, dkupris@alarm-motors.ru',
  ];

}

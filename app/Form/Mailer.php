<?php

namespace App\Form;

/**
 * Класс для отправки почты
 *
 * @author Andrew
 */
class Mailer extends \App\Config {

    /**
     * Формируем текст письма
     * @param string $subject заголовок письма
     * @param array $labels заголовки полей
     * @param array $values значения полей
     * @return string html-код письма
     */
    public function createLetter($subject, $labels, $values)
    {
        //поля которые являются ссылками
        $links = ['link', 'mail', 'email'];
        //поля которые являются электронной почтой
        $mails = ['mail', 'email'];
        $text = '';
        $text .= '<h2 style="padding-left: 30px; font-family: Helvetica, Arial, sans-serif;">' . $subject . '</h2>' . PHP_EOL;
        $text .= '<table style="margin-left: 30px; width: 600px; font-size: 14px; line-height: 22px;" rules="none">';
        foreach ($values as $k => $v) {
            $text .= '<tr>';
            $text .= '<td style="padding: 4px 14px 4px 8px; min-width: 120px; border-bottom: #ededed solid 1px; width: 200px; font: 14px/24px Arial, sans-serif;">';
            $text .= '<strong style=" width: 200px; display: inline-block; font-weight: normal; color: #727272;">' . ($labels[$k]) . '</strong>';
            $text .= '</td>' . PHP_EOL;
            $text .= '<td style="padding: 4px 14px 4px 8px; min-width: 120px; border-bottom: #ededed solid 1px; font: 14px/24px Arial, sans-serif; color: #000;">';
            $text .= in_array($k, $links) ? '<a href="' . (in_array($k, $mails) ? 'mailto:' : '') . $v . '">' : '';
            $text .= $v ?: '-';
            $text .= in_array($k, $links) ? '</a>' : '';
            $text .= '</td>' . PHP_EOL . '</tr>' . PHP_EOL;
        }
        $text .= '</table>';

        return $text;
    }

    /**
     * Добавляем SMTP настройки в mailer
     * @param PHPMailer $mail
     */
    private function addMailSMTPSettings(&$mail)
    {
        if (empty($mail)) {
            return false;
        }

        $mail->isSMTP();                            // Set mailer to use SMTP
        $mail->CharSet = 'UTF-8';                   // Encode
        $mail->Host = 'smtp.yandex.ru';             // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                     // Enable SMTP authentication
        $mail->Username = $this->mail_from;         // SMTP username
        $mail->Password = $this->mail_password;     // SMTP password
        $mail->SMTPSecure = 'ssl';                  // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                          // TCP port to connect to
    }

    /**
     * Добавляем получателей письма
     * @param PHPMailer $mail
     * @param string $list список получателей
     * @param string $type тип списка (to - получатели, cc - копия, bcc - скрытая копия)
     */
    private function addRecipient(&$mail, $list, $type)
    {
        if (empty($mail) || empty($list)) {
            return false;
        }

        $mails = $this->recipientListPrepare($list);

        foreach ($mails as $to) {
            if ($type == 'to') {
                $mail->addAddress($to);
            } elseif ($type == 'cc') {
                $mail->addCC($to);
            } elseif ($type == 'bcc') {
                $mail->addBCC($to);
            }
        }
    }

    /**
     * Подготавливаем список получателей заявок
     * @param mixed $list список получателей через ','
     * @return array список получателей в виде массива
     */
    private function recipientListPrepare($list)
    {
        $result = [];

        if (mb_stripos($list, ',', 0, 'utf-8') !== false) {
            $tmp = explode(',', $list);
            foreach ($tmp as $mail) {
                $result[] = trim($mail);
            }
        } elseif (is_array($list)) {
            $result = $list;
        } else {
            $result[] = $list;
        }

        return $result;
    }

    /**
     * Добавляем параметры отправителя в mailer
     * @param PHPMailer $mail
     */
    private function addMailSenderSettings(&$mail)
    {
        if (empty($mail)) {
            return false;
        }

        $mail->From = $this->mail_from;
        $mail->FromName = $this->mail_name;
        $mail->addReplyTo($this->mail_from, $this->mail_name);
    }

    /**
     * Добавляем контент в mailer
     * @param PHPMailer $mail
     * @param string $subject тема письма
     * @param string $content html-код письма
     */
    private function addMailContent(&$mail, $subject, $content)
    {
        if (empty($mail)) {
            return false;
        }

        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $content;
        $mail->AltBody = strip_tags($content);
    }

    /**
     * Отправка писма
     * @param string $subject тема письма
     * @param string $list список получателей
     * @param string $content контент письма
     * @param string $attach ссылка на файл вложения
     * @return string результат отправки или текст ошибки
     */
    public function sendMail($subject, $list, $content, $attach = [])
    {
        $mail = new \PHPMailer\PHPMailer\PHPMailer();

        $this->addMailSMTPSettings($mail);
        $this->addMailSenderSettings($mail);

        $this->addRecipient($mail, $list, 'to');
        $this->addRecipient($mail, $this->mail_from, 'bcc');

        if (!empty($attach)) {
          foreach($attach as $key => $pathToImage) {
            $mail->addAttachment($pathToImage);
          }
        }

        $this->addMailContent($mail, $subject, $content);

        $result = $mail->send();

        if (!$result) {
            $error = $mail->ErrorInfo;
            $mail->isMail();
            $result = $mail->send();
        }

        if (!$result) {
            $result = $error;
        }

        return $result;
    }

}

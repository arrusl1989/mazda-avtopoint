<?php

namespace App\Form;

/**
 * Валидация форм, фильтрация входящих данных
 *
 * @author Andrew
 */
class Validation extends Core {

    /**
     * Ошибки валидации формы
     * @var array
     */
    protected $form_errors = Array();

    /**
     * Получаем html-код блока ошибок
     * @return mixed код или false
     */
    public function error()
    {
        if (empty($this->form_errors)) {
            return false;
        }

        $result = '';

        foreach ($this->form_errors as $error) {
            $result .= $error;
        }

        return $result;
    }

    /**
     * Проверяем базовые поля
     * @param array $income_data входные данные
     * @return boolean
     */
    protected function validateDefault($income_data)
    {
        $result = true;

//        if ($income_data['token'] !== $this->getToken()) {
//            $result = false;
//            $this->form_errors[] = 'Неверный токен формы';
//        }

        if (empty($income_data['action'])) {
            $result = false;
            $this->form_errors[] = 'Неизвестная ошибка';
        }

        return $result;
    }

    /**
     * Валидация полученных данных формы
     * @param array $income_data данные формы
     * @return boolean
     */
    protected function validate($income_data)
    {
      $result = $this->validateDefault($income_data);

      $data = $this->baseData($income_data['action']);

      foreach ($data['field'] as $field_code => $field_data) {
        if (!$field_data['required']) {
          continue;
        }

        $val = $income_data[$field_code];

        if (empty($val) && (!is_numeric($val) || intval($val) !== 0) || $val === '+7 (___) ___-__-__') {
          $result = false;
          $this->form_errors[] = 'Не заполнено обязательное поле «' . strip_tags($field_data['label']) . '»';
        }
      }

      return $result;
    }

    /**
     * Массив фильтрации для filter_input
     * @param string $form_name имя формы с названием модуля
     * @return array массив фильтрации
     */
    public function filterArray($form_name)
    {
        $form_data = $this->baseData($form_name);

        $string = $this->filterType('string');

        $filter = [];

        foreach ($this->default_field as $v) {
          $filter[$v] = $string;
        }

        foreach ($form_data['field'] as $name => $field) {
            $filter[$name] = $this->filterType($field['format']);
        }

        return $filter;
    }

    /**
     * Преобразовываем тип поля на значение для функции filter_input
     * @param string $type тип поля
     * @return string тип поля для filter_input
     */
    protected function filterType($type)
    {
        $list = Array(
            'email' => FILTER_VALIDATE_EMAIL,
            'string' => FILTER_SANITIZE_STRING,
            'html' => Array(
                'filter' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
                'flags' => FILTER_FLAG_NO_ENCODE_QUOTES,
            ),
            'int' => FILTER_VALIDATE_INT,
            'array' => Array(
                'filter' => FILTER_VALIDATE_INT,
                'flags' => FILTER_REQUIRE_ARRAY,
            ),
            'text' => Array(
                'filter' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
                'flags' => FILTER_REQUIRE_ARRAY,
            ),
        );

        return $list[$type] ?: FILTER_SANITIZE_STRING;
    }

}

<?php

namespace App\Form;

/**
 * Основа для форм
 *
 * @author Andrew
 */
class Core extends \App\Config {

    /**
     * Поля по-умолчанию
     * @var array
     */
    protected $default_field = [
      'action',
    ];

    /**
     * Получаем данные формы
     * @param string $name имя формы
     * @return array массив <br />
     * module (string) - имя модуля <br />
     * method (string) - имя метода
     */
    protected function baseData($name)
    {
        return $this->form_list[$name];
    }


}

<?php

namespace App\Form;

class Uploader
{
  /**
   * Флаг корректности файла
   *
   * @var bool
   */
  protected $correctFile = false;

  /**
   * Флаг загрузки
   *
   * @var bool
   */
  protected $upload = false;

  /**
   * Сообщение об ошибке
   *
   * @var string
   */
  protected $errorMessage = '';

  /**
   * Список разрешенных расширений
   * для загружаемого файла
   *
   * @var array
   */
  private $allowedExtensions = [
    'jpg',
    'jpeg',
    'png'
  ];

  /**
   * Имя загруженного файла
   * без расширения
   *
   * @var array
   */
  private $fileNames;

  /**
   * Расширение загруженного файла
   *
   * @var array
   */
  private $extensions;

  /**
   * Список путей до файлов
   *
   * @var array
   */
  private $pathToFiles;

  /**
   * Путь до папки с загрузками
   *
   * @var string
   */
  private $pathToDownloadsFolder = __DIR__ . '/../../downloads/';

  public function __construct()
  {
    if (empty($_FILES) || $this->isFileNotUpload()) {
        $this->pathToFiles = [];
    } else if ($this->isUploadAndNotError()) {
      $this->upload = true;
      $this->fileNames = $this->getFileName();
      $this->extensions = $this->getExtensions();

      if ($this->isValidExtension()) {
        $fileNames = $this->createUniqueName();

        foreach ($fileNames as $name) {
          $this->pathToFiles[] = $this->pathToDownloadsFolder . $name;
        }

        $this->moveUploadTempFiles();
        $this->correctFile = true;
      } else {
        $this->pathToFiles = 'Вы пытаетесь отправить некорректный файл';
      }
    } else {
      $this->pathToFiles = 'Вы пытаетесь отправить некорректный файл';
    }
  }

  /**
   * Получить путь до файла
   *
   * @return string
   */
  public function getPathToFiles()
  {
    return $this->pathToFiles;
  }

  /**
   * Удалить загруженный
   * перемещенный файл
   *
   */
  public function deleteMovedFiles()
  {

    $files = array_diff(scandir($this->pathToDownloadsFolder), ['..', '.', '.gitkeep']);

    foreach ($files as $file) {
      unlink($this->pathToDownloadsFolder . $file);
    }

  }

  /**
   * Загружен ли файл
   *
   * @return bool
   */
  public function isUpload()
  {
    return $this->upload;
  }

  /**
   * Соответствует ли файл
   * критериям
   *
   * @return bool
   */
  public function isCorrectFile()
  {
    return $this->correctFile;
  }

  /**
   * Получить текст ошибки
   *
   * @return string
   */
  public function getErrorMessage()
  {
    return $this->errorMessage;
  }

  /**
   * Получить имя файла
   *
   * @return array
   */
  private function getFileName()
  {
    $names = [];

    foreach ($_FILES as $name => $content) {
      foreach ($content['name'] as $key => $name){
        $names[] = pathinfo($name, PATHINFO_FILENAME);
      }
    }

    return $names;
  }

  /**
   * Получить расширения загружаемых файлов
   *
   * @return array
   */
  private function getExtensions()
  {
    $extensions = [];

    foreach ($_FILES as $name => $content) {
      foreach ($content['name'] as $key => $name){
        $extensions[] = pathinfo($name, PATHINFO_EXTENSION);
      }
    }
    return $extensions;
  }

  /**
   * Были ли добавлены файлы при отправке формы
   * (на случай если добавление изображений необязательно)
   *
   * @return bool
   */
  private function isFileNotUpload()
  {
    $errorCodeNotUpload = 4;

    foreach ($_FILES as $name => $content) {
      foreach ($content['error'] as $key => $name){
        if ($name == $errorCodeNotUpload) {
          return true;
        }
      }
    }

    return false;
  }

  /**
   * Проверка на удачную
   * загрузку файла
   *
   * @return bool
   */
  private function isUploadAndNotError()
  {
    $result = true;

    foreach ($_FILES as $name => $content) {
      foreach ($content['error'] as $key => $error) {
        if ($error !== 0) {
          $result = false;
        }
      }
    }

    return $result;
  }

  /**
   * Переместить временный файл
   *
   */
  private function moveUploadTempFiles()
  {
    $i = 0;
    foreach ($_FILES as $name => $content) {
      foreach ($content['tmp_name'] as $key => $file) {
        if (!move_uploaded_file($file, $this->pathToFiles[$i])) {
          return exit("$file не перемещен");
        };
        $i++;
      }
    }
  }

  /**
   * Создать уникальное имя для файла
   *
   * @return array
   */
  private function createUniqueName()
  {
    foreach ($this->fileNames as $key => $names) {
      $result[] = $names . uniqid('_') . '.' . $this->extensions[$key];
    }
    return $result;
  }

  /**
   * Проверка на расширение
   *
   * @return bool
   */
  private function isValidExtension()
  {
    foreach ($this->extensions as $key => $ext) {
      if (!in_array($ext, $this->allowedExtensions)) {
        return false;
      }
    }
    return true;
  }

}

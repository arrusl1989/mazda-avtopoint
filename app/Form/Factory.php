<?php

namespace App\Form;

/**
 * Создание отдельных форм на странице, их обработка и валидация
 *
 * @author Andrew
 */
class Factory extends Validation {

    /**
     * Обрабатываем форму
     * @param array $income_data данные формы
     * @return array результат обработки
     */
    public function processing($income_data)
    {
        $data = $this->clean($income_data);

        if (!$this->validate($data)) {
            $result = Array(
                'status' => 'error',
                'content' => $this->error()
            );
        } else {
            $result = Array(
                'status' => 'success',
                'data' => $data
            );
        }

        return $result;
    }

    /**
     * Удаляем из формы недоступные для заполнения поля
     * @param array $income_data массив входящих данных
     * @return array очищенный массив
     */
    protected function clean($income_data)
    {
        $data = $this->baseData($income_data['action']);

        foreach (array_keys($income_data) as $k) {
            $field = $data['field'][$k];

            if (!empty($field['role']) && $this->userData('role') < $field['role']) {
                unset($income_data[$k]);
            }
        }

        return $income_data;
    }

}

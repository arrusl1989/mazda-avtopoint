<?php

namespace App\Form;

/**
 * Класс для работы с формами
 *
 * @author Andrew
 */
class Form extends Core {

    /**
     * Данные текущей формы
     * @var array
     */
    protected $form_data = Array();

    /**
     * Создаем новую форму
     * @param string $name имя формы
     * @return \Form\Form
     */
    public function __construct($name)
    {
        $this->form_data['data'] = $this->baseData($name);
        $this->form_data['action'] = $this->form_data['data']['name'] = $name;

        if (empty($this->form_data['data'])) {
            $this->form_data['not_exists'] = true;
            return $this;
        }

        return $this;
    }

    /**
     * Добавить данные в значения полей
     * @param array $data массив данных вида "имя поля" => "значение"
     * @return \Form\Form
     */
    public function data($data)
    {
        if (empty($data) || !is_array($data)) {
            return $this;
        }

        foreach ($data as $k => $v) {
            $this->form_data['data']['field'][$k]['value'] = $v;
        }

        return $this;
    }

    /**
     * Добавить поля в форму
     * @param array $data массив данных
     * @return \Form\Form
     */
    public function field($data)
    {
        if (empty($data) || !is_array($data)) {
            return $this;
        }

        foreach ($data as $k => $sub) {
            foreach ($sub as $sk => $sv) {
                $this->form_data['data']['field'][$k][$sk] = $sv;
            }
        }

        return $this;
    }

    /**
     * Получаем метки полей для формы
     * @return array массив названий полей
     */
    protected function getLabels()
    {
        $field = $this->form_data['data']['field'];

        $result = [];

        foreach ($field as $k => $v) {
            $result[$k] = strip_tags($v['label']);
        }

        return $result;
    }

  /**
   * Отправляем заявку на почту
   *
   * @param array $data массив полей формы
   * @param array $images
   *
   * @return mixed true при успешной отправке string текст ошибки
   */
    public function sendLetter($data, $images = [])
    {
        $name = $data['action'];
        unset($data['action']);

        $label = $this->getLabels();
        $subject = $this->form_data['data']['title'];

        $mailer = new Mailer();
        $letter = $mailer->createLetter($subject, $label, $data);

        $recipient = $this->form_contacts[$name];
        if (empty($recipient)) {
            $recipient = $this->form_contacts['default'];
        }

        $result = $mailer->sendMail($subject, $recipient, $letter, $images);

        //если заявка отправилась - отправляем данные в crm
        if ($result === true) {
//            $crm = new \Core\FourPx\Meta\Crm();

            $data['subject'] = $subject;

//            $crm->sendDataCRM($data);
        }

        return $result;
    }

}

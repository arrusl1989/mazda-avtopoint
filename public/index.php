<?php

mb_internal_encoding('utf-8');

/**
 * Корневая дирректория проекта
 * @var string
 */
define('ROOT_PATH', __DIR__);

//автозагрузка средствами composer
require_once ROOT_PATH . '/../vendor/autoload.php';

$router = new \App\Tools\Router();

$class = $router->getClass();

$ctrl = new $class;
$ctrl();

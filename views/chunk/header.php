<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <title><?= $title; ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="./_assets/module/normalize/normalize.css">
        <link rel="stylesheet" href="./_assets/module/owl-carousel/owl.carousel.min.css">
        <link rel="stylesheet" href="./_assets/module/fancybox/jquery.fancybox.min.css">
        <link rel="stylesheet" href="./_assets/module/formstyler/jquery.formstyler.css">
        <link rel="stylesheet" href="./_assets/module/formstyler/jquery.formstyler.theme.css">
        <link rel="stylesheet" href="./_assets/css/style.css<?= $version; ?>">
        <link rel="icon" type="image/png" href="./_assets/favicon/favicon.png" />
        <!-- coMagic -->
        <script type="text/javascript">
            var __cs = __cs || [];
            __cs.push(["setCsAccount", "nBdHotTZ75MuA7viEvkpsIqX6yeyOV6w"]);
        </script>
        <script type="text/javascript" async src="https://app.comagic.ru/static/cs.min.js"></script>
        <!-- /coMagic -->

        <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
            (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
            (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
            ym(65845357, "init", {
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true
            });
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/65845357" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
        
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-173379150-1"></script>
            <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
                gtag('config', 'UA-173379150-1');
            </script>
    </head>

    <body class="body">
        <div class="wrapper">

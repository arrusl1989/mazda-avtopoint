<footer class="footer">
    <div class="container">
        <div class="footer__wrap">
            <div class="footer__row"> <a href="#" class="footer__logo">Аларм-Моторс</a>
                <div class="footer__socials">
                    <a href="https://vk.com/apmazda" class="footer__socials-item" target="_blank">
                        <img class="socials_img" src="./_assets/img/style/icons/icon_soc1.svg" alt="vk_ico">
                    </a>
                    <a href="https://www.facebook.com/apmazda/" class="footer__socials-item" target="_blank">
                        <img class="socials_img" src="./_assets/img/style/icons/icon_soc2.svg" alt="fb_ico">
                    </a>
                    <a href="https://www.instagram.com/avtopoint_mazda/" class="footer__socials-item" target="_blank">
                        <img class="socials_img" src="./_assets/img/style/icons/icon_soc3.svg" alt="inst_ico">
                    </a>
                    <a href="https://www.youtube.com/user/avtopointmazda" class="footer__socials-item" target="_blank">
                        <img class="socials_img" src="./_assets/img/style/icons/icon_soc4.svg" alt="you_ico">
                    </a>
                </div>
            </div>
            <div class="footer__descr">
                <p>Общество с ограниченной ответственностью «Аларм-Комтранс»</p>
                <p>Обращаем Ваше внимание на то, что вся представленная на сайте информация, касающаяся комплектаций, технических характеристик, цветовых сочетаний, а также стоимости автомобилей и сервисного обслуживания носит информационный характер и ни при каких условиях не является публичной офертой, определяемой положениями Статьи 437 (2) Гражданского кодекса Российской Федерации. Для получения подробной информации, пожалуйста, обращайтесь в Дилерский Центр «Аларм Моторс».</p> 
                <br /> <a href="/policy">Политика безопасности</a>
            </div>
        </div>
    </div>
</footer>
<div class="modal" id="master1">
    <div class="modal__close" data-fancybox-close>
        <svg>
            <use xlink:href="#icon_close" />
        </svg>
    </div>
    <div class="modal__wrap">
        <div class="masters__item">
            <div class="masters__row">
                <div class="masters__image">
                    <div class="masters__image-wrap">
                        <img src="./_assets/img/content/masters_image1.jpg" alt="">
                    </div>
                </div>
                <div class="masters__info">
                    <div class="masters__name">Олег Калганов</div>
                    <div class="masters__job">Диагност – электрик</div>
                    <div class="masters__rating">
                        <div class="rating">
                            <div class="rating__item active">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                            <div class="rating__item active">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                            <div class="rating__item active">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                            <div class="rating__item active">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                            <div class="rating__item">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="masters__row masters__row_fs">
                <div class="masters__data">
                    <div class="masters__data-num">10 лет</div>
                    <div class="masters__data-descr">Опыт работы</div>
                </div>
                <div class="masters__data">
                    <div class="masters__data-num">8 лет</div>
                    <div class="masters__data-descr">Опыт ремонта и
                        <br />обслуживания Мазда</div>
                </div>
            </div>
        </div>
        <div class="modal__title">Обучение в учебном центре Мазда Мотор Рус:</div>
        <div class="modal__ul">
            <ul>
                <li>Электрические и электронные системы MAZDA. Принципы работы. Поиск неисправностей</li>
                <li>Двигатели SKYACTIV</li>
                <li>Курс «Особенности обслуживания автомобилей SKYACTIV»</li>
                <li>Курс «Электрические и электронные системы MAZDA».</li>
                <li>Основы конструкции и ремонта автоматических трансмиссий MAZDA</li>
                <li>Диагностика электрических и электронных систем MAZDA.</li>
                <li>MTC_58e Mazda3</li>
                <li>MTC_56e Mazda CX-5_2017</li>
                <li>MTC_218e Heating, Ventilation and Air Conditioning</li>
                <li>MTC_114e Manual Transaxle + 4WD</li>
            </ul>
        </div>
        <div class="modal__title">Дипломы:</div>
        <div class="modal__images">
            <div class="modal__image">
                <img src="./_assets/img/content/diplom_image1.png" alt="">
            </div>
            <div class="modal__image">
                <img src="./_assets/img/content/diplom_image2.png" alt="">
            </div>
        </div>
        <div class="modal__title">Профессиональные навыки и специализация:</div>
        <div class="modal__ul">
            <ul>
                <li>- Диагностика электронных компонентов автомобилей Мазда;</li>
                <li>- Поиск и устранение «сложных» электрических неисправностей на автомобилях
                    с большими пробегами за один визит;</li>
                <li>- Опыт ремонта установки и обслуживания сигнализаций и дополнительного
                    противоугонного оборудования;</li>
                <li>- Ремонт ходовой части автомобиля;</li>
                <li>- Ремонт и облуживание тормозных систем, систем АБС;</li>
                <li>- Ремонт рулевого управления, электроусилителей ЭУР;</li>
                <li>- Ремонт и обслуживание кондиционеров;</li>
                <li>- Обновление ПО систем автомобиля, навигационных карт, добавление функций;</li>
                <li>- Комплексное техническое обслуживание автомобилей Мазда.</li>
            </ul>
            <p><strong>Специализация</strong> – диагностика электрооборудования автомобилей Мазда</p>
        </div>
        <div class="modal__title">Отремонтировано и обслужено автомобилей в 2019 году:</div>
        <div class="modal__list">
            <div class="modal__list-item">
                <div class="modal__list-value">15 а/м</div>
                <div class="modal__list-label">Mazda 2</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">75 а/м</div>
                <div class="modal__list-label">Mazda 3</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">176 а/м</div>
                <div class="modal__list-label">Mazda 6</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">344 а/м</div>
                <div class="modal__list-label">Mazda CX-5</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">20 а/м</div>
                <div class="modal__list-label">Mazda CX-7</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">8 а/м</div>
                <div class="modal__list-label">Mazda CX-9</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">12 а/м</div>
                <div class="modal__list-label">Mazda BT-50</div>
            </div>
        </div>
        <div class="modal__button"> <a href="#master_order" data-fancybox data-name="Олег Калганов" class="button button_full js-master-name"><span>записаться к мастеру</span></a>
        </div>
    </div>
</div>

<div class="modal" id="master2">
    <div class="modal__close" data-fancybox-close>
        <svg>
            <use xlink:href="#icon_close" />
        </svg>
    </div>
    <div class="modal__wrap">
        <div class="masters__item">
            <div class="masters__row">
                <div class="masters__image">
                    <div class="masters__image-wrap">
                        <img src="./_assets/img/content/masters_image2.jpg" alt="">
                    </div>
                </div>
                <div class="masters__info">
                    <div class="masters__name">Мстислав Нефляшев</div>
                    <div class="masters__job">Автомеханик-агрегатчик</div>
                    <div class="masters__rating">
                        <div class="rating">
                            <div class="rating__item active">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                            <div class="rating__item active">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                            <div class="rating__item active">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                            <div class="rating__item active">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                            <div class="rating__item">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="masters__row masters__row_fs">
                <div class="masters__data">
                    <div class="masters__data-num">7 лет</div>
                    <div class="masters__data-descr">Опыт работы</div>
                </div>
                <div class="masters__data">
                    <div class="masters__data-num">7 лет</div>
                    <div class="masters__data-descr">Опыт ремонта и
                        <br />обслуживания Мазда</div>
                </div>
            </div>
        </div>
        <div class="modal__title">Обучение в учебном центре Мазда Мотор Рус:</div>
        <div class="modal__ul">
            <ul>
                <li>Особенности обслуживания автомобилей SKYACTIV (SKYACTIV Maintenance Course)</li>
                <li>Подвески MAZDA и технологии SKYACTIV</li>
                <li>Электрические и электронные системы MAZDA. Принципы работы. Поиск неисправностей</li>
                <li>Двигатели SKYACTIV</li>
                <li>MTC_114e Manual Transaxle + 4WD</li>
                <li>MTC_316e (RU) Automatic Transaxle Diagnostics</li>
                <li>MTC_207e - Sensors, Actuators & Data Bus Systems (Technical Training)</li>
                <li>MTC_43e Mazda CX-5</li>
            </ul>
        </div>
        <div class="modal__title">Дипломы:</div>
        <div class="modal__images">
            <div class="modal__image">
                <img src="./_assets/img/content/diplom_image3.png" alt="">
            </div>
            <div class="modal__image">
                <img src="./_assets/img/content/diplom_image4.png" alt="">
            </div>
        </div>
        <div class="modal__title">Профессиональные навыки и специализация:</div>
        <div class="modal__ul">
            <ul>
                <li>- Диагностика и агрегатный ремонт двигателей, трансмиссий, ремонт турбонагнетателей, замена цепей двигателя;</li>
                <li>- Диагностика и ремонт ходовой части автомобиля;</li>
                <li>- Ремонт и облуживание тормозных систем, переборка тормозных механизмов;</li>
                <li>- Ремонт рулевого управления, регулировка развал/схождения, калибровка и регулировка электроусилителей рулевого управления;</li>
                <li>- Спортивная настройка подвески;</li>
                <li>- Ремонт и обслуживание кондиционеров, климат-систем автомобилей Мазда;</li>
                <li>- Комплексное техническое обслуживание автомобилей.</li>
            </ul>
            <p><strong>Узкая специализация</strong> – опыт работы со спортивными автомобилями Mazda 3,6 MPS, знание особенностей конструкции технологии SKYACTIV, переборка механических трансмиссий, капитальный ремонт двигателей Мазда.</p>
        </div>
        <div class="modal__title">Отремонтировано и обслужено автомобилей в 2019 году:</div>
        <div class="modal__list">
            <div class="modal__list-item">
                <div class="modal__list-value">2 а/м</div>
                <div class="modal__list-label">Mazda MX-5</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">45 а/м в т.ч. 5 MPS</div>
                <div class="modal__list-label">Mazda 3</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">150 а/м</div>
                <div class="modal__list-label">Mazda 6</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">274 а/м</div>
                <div class="modal__list-label">Mazda CX-5</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">45 а/м</div>
                <div class="modal__list-label">Mazda CX-7</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">41 а/м</div>
                <div class="modal__list-label">Mazda CX-9</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">33 а/м</div>
                <div class="modal__list-label">Mazda BT-50</div>
            </div>
        </div>
        <div class="modal__button"> <a href="#master_order" data-fancybox data-name="Мстислав Нефляшев" class="button button_full js-master-name"><span>записаться к мастеру</span></a>
        </div>
    </div>
</div>

<div class="modal" id="master3">
    <div class="modal__close" data-fancybox-close>
        <svg>
            <use xlink:href="#icon_close" />
        </svg>
    </div>
    <div class="modal__wrap">
        <div class="masters__item">
            <div class="masters__row">
                <div class="masters__image">
                    <div class="masters__image-wrap">
                        <img src="./_assets/img/content/masters_image3.jpg" alt="">
                    </div>
                </div>
                <div class="masters__info">
                    <div class="masters__name">Юрий Васильев</div>
                    <div class="masters__job">Автомеханик-диагност</div>
                    <div class="masters__rating">
                        <div class="rating">
                            <div class="rating__item active">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                            <div class="rating__item active">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                            <div class="rating__item active">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                            <div class="rating__item active">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                            <div class="rating__item">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="masters__row masters__row_fs">
                <div class="masters__data">
                    <div class="masters__data-num">22 года</div>
                    <div class="masters__data-descr">Опыт работы</div>
                </div>
                <div class="masters__data">
                    <div class="masters__data-num">13 лет</div>
                    <div class="masters__data-descr">Опыт ремонта и
                        <br />обслуживания Мазда</div>
                </div>
            </div>
        </div>
        <div class="modal__title">Обучение в учебном центре Мазда Мотор Рус:</div>
        <div class="modal__ul">
            <ul>
                <li>Двигатели SKYACTIV</li>
                <li>Курс «Особенности обслуживания автомобилей SKYACTIV»</li>
                <li>Основы конструкции и ремонта автоматических трансмиссий MAZDA</li>
                <li>Курс «Электрические и электронные системы MAZDA»</li>
                <li>Курс «Подвески MAZDA и технологии SKYACTIV»</li>
                <li>Heating, Ventilation and Air Conditioning</li>
                <li>Electric Systems 1</li>
                <li>Sensors, Actuators & Data Bus Systems (Technical Training)</li>
                <li>(RU) Automatic Transaxle Diagnostics</li>
                <li>Mazda CX-5_2017</li>
            </ul>
        </div>
        <!-- <div class="modal__title">Дипломы:</div>
        <div class="modal__images">
            <div class="modal__image">
                <img src="./_assets/img/content/diplom_image3.png" alt="">
            </div>
            <div class="modal__image">
                <img src="./_assets/img/content/diplom_image4.png" alt="">
            </div>
        </div> -->
        <div class="modal__title">Профессиональные навыки и специализация:</div>
        <div class="modal__ul">
            <ul>
                <li>- Диагностика и агрегатный ремонт двигателей, КПП;</li>
                <li>- Диагностика и ремонт ходовой части автомобиля;</li>
                <li>- Ремонт и облуживание тормозных систем;</li>
                <li>- Ремонт рулевого управления, регулировка развал/схождения;</li>
                <li>- Диагностика электронных компонентов автомобилей Мазда;</li>
                <li>- Ремонт и обслуживание кондиционеров, климат-систем автомобилей Мазда.</li>
            </ul>
            <p><strong>Узкая специализация</strong> – опыт работы с автомобилями Mazda CX-7 знание особенностей конструкции узлов с момента выхода модели на рынок.</p>
        </div>
        <div class="modal__title">Отремонтировано и обслужено автомобилей в 2019 году:</div>
        <div class="modal__list">
            <div class="modal__list-item">
                <div class="modal__list-value">2 а/м</div>
                <div class="modal__list-label">Mazda 2</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">70 а/м</div>
                <div class="modal__list-label">Mazda 3</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">156 а/м</div>
                <div class="modal__list-label">Mazda 6</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">334 а/м</div>
                <div class="modal__list-label">Mazda CX-5</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">53 а/м</div>
                <div class="modal__list-label">Mazda CX-7</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">31 а/м</div>
                <div class="modal__list-label">Mazda CX-9</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">6 а/м</div>
                <div class="modal__list-label">Mazda BT-50</div>
            </div>
        </div>
        <div class="modal__button"> <a href="#master_order" data-fancybox data-name="Юрий Васильев" class="button button_full js-master-name"><span>записаться к мастеру</span></a>
        </div>
    </div>
</div>

<div class="modal" id="master4">
    <div class="modal__close" data-fancybox-close>
        <svg>
            <use xlink:href="#icon_close" />
        </svg>
    </div>
    <div class="modal__wrap">
        <div class="masters__item">
            <div class="masters__row">
                <div class="masters__image">
                    <div class="masters__image-wrap">
                        <img src="./_assets/img/content/masters_image4.jpg" alt="">
                    </div>
                </div>
                <div class="masters__info">
                    <div class="masters__name">Сергей Шмелев</div>
                    <div class="masters__job">Диагност-электрик</div>
                    <div class="masters__rating">
                        <div class="rating">
                            <div class="rating__item active">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                            <div class="rating__item active">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                            <div class="rating__item active">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                            <div class="rating__item active">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                            <div class="rating__item">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="masters__row masters__row_fs">
                <div class="masters__data">
                    <div class="masters__data-num">21 год</div>
                    <div class="masters__data-descr">Опыт работы</div>
                </div>
                <div class="masters__data">
                    <div class="masters__data-num">12 лет</div>
                    <div class="masters__data-descr">Опыт ремонта и
                        <br />обслуживания Мазда</div>
                </div>
            </div>
        </div>
        <div class="modal__title">Обучение в учебном центре Мазда Мотор Рус:</div>
        <div class="modal__ul">
            <ul>
                <li>new MAZDA3 (BP)</li>
                <li>Диагностика электрических и электронных систем MAZDA</li>
                <li>Двигатели SKYACTIV</li>
                <li>Основы конструкции и ремонта автоматических трансмиссий MAZDA</li>
                <li>Особенности обслуживания автомобилей SKYACTIV</li>
                <li>Курс «Электрические и электронные системы MAZDA»</li>
                <li>Курс «Подвески MAZDA и технологии SKYACTIV»</li>
                <li>Двигатели SKYACTIV</li>
                <li>CX-7 Online Training (Russia)</li>
                <li>Heating, Ventilation and Air Conditioning</li>
                <li>Sensors, Actuators & Data Bus Systems (Technical Training)</li>
                <li>Automatic Transaxle Diagnostics</li>
                <li>Manual Transaxle + 4WD</li>
            </ul>
        </div>
        <!-- <div class="modal__title">Дипломы:</div>
        <div class="modal__images">
            <div class="modal__image">
                <img src="./_assets/img/content/diplom_image3.png" alt="">
            </div>
            <div class="modal__image">
                <img src="./_assets/img/content/diplom_image4.png" alt="">
            </div>
        </div> -->
        <div class="modal__title">Профессиональные навыки и специализация:</div>
        <div class="modal__ul">
            <ul>
                <li>- Диагностика электронных компонентов автомобилей Мазда;</li>
                <li>- Поиск и устранение «сложных» электрических неисправностей на автомобилях с большими пробегами за один визит;</li>
                <li>- Диагностика и агрегатный ремонт двигателей,  ремонт ГБЦ, замена цепей ГРМ, переборка турбонагнетателя;</li>
                <li>- Ремонт ходовой части автомобиля;</li>
                <li>- Ремонт и облуживание тормозных систем, систем АБС;</li>
                <li>- Ремонт рулевого управления, электроусилителей ЭУР;</li>
                <li>- Ремонт и обслуживание кондиционеров;</li>
                <li>- Обновление ПО систем автомобиля, навигационных карт, добавление функций;</li>
                <li>- Комплексное техническое обслуживание автомобилей Мазда.</li>
            </ul>
            <p><strong>Узкая специализация</strong> – опыт работы с автомобилями Mazda CX-7, BT-50, RX-8, знание особенностей этих моделей с момента их выхода на рынок.</p>
        </div>
        <div class="modal__title">Отремонтировано и обслужено автомобилей в 2019 году:</div>
        <div class="modal__list">
            <div class="modal__list-item">
                <div class="modal__list-value">11 а/м</div>
                <div class="modal__list-label">Mazda 2</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">85 а/м</div>
                <div class="modal__list-label">Mazda 3</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">146 а/м</div>
                <div class="modal__list-label">Mazda 6</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">240 а/м</div>
                <div class="modal__list-label">Mazda CX-5</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">28 а/м</div>
                <div class="modal__list-label">Mazda CX-7</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">17 а/м</div>
                <div class="modal__list-label">Mazda CX-9</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">12 а/м</div>
                <div class="modal__list-label">Mazda BT-50</div>
            </div>
        </div>
        <div class="modal__button"> <a href="#master_order" data-fancybox data-name="Сергей Шмелев" class="button button_full js-master-name"><span>записаться к мастеру</span></a>
        </div>
    </div>
</div>

<div class="modal" id="master5">
    <div class="modal__close" data-fancybox-close>
        <svg>
            <use xlink:href="#icon_close" />
        </svg>
    </div>
    <div class="modal__wrap">
        <div class="masters__item">
            <div class="masters__row">
                <div class="masters__image">
                    <div class="masters__image-wrap">
                        <img src="./_assets/img/content/masters_image5.jpg" alt="">
                    </div>
                </div>
                <div class="masters__info">
                    <div class="masters__name">Алексей Рясянен</div>
                    <div class="masters__job">Автомеханик-агрегатчик</div>
                    <div class="masters__rating">
                        <div class="rating">
                            <div class="rating__item active">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                            <div class="rating__item active">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                            <div class="rating__item active">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                            <div class="rating__item active">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                            <div class="rating__item">
                                <svg>
                                    <use xlink:href="#icon_star" />
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="masters__row masters__row_fs">
                <div class="masters__data">
                    <div class="masters__data-num">21 год</div>
                    <div class="masters__data-descr">Опыт работы</div>
                </div>
                <div class="masters__data">
                    <div class="masters__data-num">12 лет</div>
                    <div class="masters__data-descr">Опыт ремонта и
                        <br />обслуживания Мазда</div>
                </div>
            </div>
        </div>
        <div class="modal__title">Обучение в учебном центре Мазда Мотор Рус:</div>
        <div class="modal__ul">
            <ul>
                <li>Электрические и электронные системы MAZDA. Принципы работы. Поиск неисправностей</li>
                <li>Подвески MAZDA и технологии SKYACTIV</li>
                <li>Курс «Особенности обслуживания автомобилей SKYACTIV»</li>
                <li>Двигатели SKYACTIV</li>
                <li>Курс «Электрические и электронные системы MAZDA»</li>
                <li>MTC_56e Mazda CX-5_2017</li>
                <li>MTC_316e (RU) Automatic Transaxle Diagnostics</li>
                <li>MTC_312e Diesel Engine Management and Diagnostics 2</li>
                <li>MTC_114e Manual Transaxle + 4WD</li>
            </ul>
        </div>
        <div class="modal__title">Дипломы:</div>
        <div class="modal__images">
            <div class="modal__image">
                <img src="./_assets/img/content/diplom_image5.png" alt="">
            </div>
            <div class="modal__image">
                <img src="./_assets/img/content/diplom_image6.png" alt="">
            </div>
        </div>
        <div class="modal__title">Профессиональные навыки и специализация:</div>
        <div class="modal__ul">
            <ul>
                <li>- Диагностика и агрегатный ремонт двигателей, трансмиссий, ремонт турбонагнетателей, замена цепей двигателя;</li>
                <li>- Диагностика и ремонт ходовой части автомобиля;</li>
                <li>- Ремонт и облуживание тормозных систем, переборка тормозных механизмов;</li>
                <li>- Ремонт рулевого управления, регулировка развал/схождения;</li>
                <li>- Ремонт и обслуживание кондиционеров, климат-систем автомобилей Мазда;</li>
                <li>- Комплексное техническое обслуживание автомобилей;</li>
                <li>- Шиномонтаж и балансировка колес с низким профилем резины.</li>
            </ul>
            <p><strong>Узкая специализация</strong> – опыт работы с полноприводными автомобилями, дизельными двигателями, знание особенностей конструкции технологии SKYACTIV, переборка тормозных систем, автоматических трансмиссий.</p>
        </div>
        <div class="modal__title">Отремонтировано и обслужено автомобилей в 2019 году:</div>
        <div class="modal__list">
            <div class="modal__list-item">
                <div class="modal__list-value">12 а/м</div>
                <div class="modal__list-label">Mazda 2</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">61 а/м</div>
                <div class="modal__list-label">Mazda 3</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">124 а/м</div>
                <div class="modal__list-label">Mazda 6</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">261 а/м</div>
                <div class="modal__list-label">Mazda CX-5</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">70 а/м</div>
                <div class="modal__list-label">Mazda CX-7</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">45 а/м</div>
                <div class="modal__list-label">Mazda CX-9</div>
            </div>
            <div class="modal__list-item">
                <div class="modal__list-value">25 а/м</div>
                <div class="modal__list-label">Mazda BT-50</div>
            </div>
        </div>
        <div class="modal__button"> <a href="#master_order" data-fancybox data-name="Алексей Рясянен" class="button button_full js-master-name"><span>записаться к мастеру</span></a>
        </div>
    </div>
</div>

<div class="modal modal_small" id="service">
    <div class="modal__close" data-fancybox-close>
        <svg>
            <use xlink:href="#icon_close" />
        </svg>
    </div>
    <div class="modal__title modal__title_big">Записаться на сервис</div>
    <form action="" class="modal__form js-ajax-form">
        <input type="hidden" name="action" value="service">
        <div class="modal__form-row">
            <input type="text" class="input-text" name="name" placeholder="Имя">
        </div>
        <div class="modal__form-row required">
            <input type="text" class="input-text js-masked" name="phone" placeholder="Телефон">
        </div>
        <div class="modal__form-row">
            <button type="submit" class="button button_full">Отправить</button>
        </div>
    </form>
</div>
<div class="modal modal_small" id="service_order">
    <div class="modal__close" data-fancybox-close>
        <svg>
            <use xlink:href="#icon_close" />
        </svg>
    </div>
    <div class="modal__title modal__title_big">Записаться на сервис</div>
    <form action="" class="modal__form js-ajax-form">
        <input type="hidden" name="action" value="service_order">
        <input type="hidden" name="services-list" class="js-services-list">
        <input type="hidden" name="services-price" class="js-services-price">
        <div class="modal__form-row">
            <input type="text" class="input-text" name="name" placeholder="Имя">
        </div>
        <div class="modal__form-row required">
            <input type="text" class="input-text js-masked" name="phone" placeholder="Телефон">
        </div>
        <div class="modal__form-row">
            <button type="submit" class="button button_full">Узнать стоимость</button>
        </div>
    </form>
</div>
<div class="modal modal_small" id="master_order">
    <div class="modal__close" data-fancybox-close>
        <svg>
            <use xlink:href="#icon_close" />
        </svg>
    </div>
    <div class="modal__title modal__title_big">Записаться к мастеру</div>
    <form action="" class="modal__form js-ajax-form">
        <input type="hidden" name="action" value="master">
        <input type="hidden" name="master" class="js-master-val">
        <div class="modal__form-row">
            <input type="text" class="input-text error" name="name" placeholder="Имя">
        </div>
        <div class="modal__form-row required">
            <input type="text" class="input-text js-masked" name="phone" placeholder="Телефон">
        </div>
        <div class="modal__form-row">
            <button type="submit" class="button button_full">Записаться</button>
        </div>
    </form>
</div>
<div class="modal modal_small" id="spin_form">
    <div class="modal__close" data-fancybox-close>
        <svg>
            <use xlink:href="#icon_close" />
        </svg>
    </div>
    <div class="modal__title modal__title_big">ИСПЫТАЙТЕ УДАЧУ</div>
    <form action="" class="modal__form js-ajax-form">
        <input type="hidden" name="action" value="luck">
        <input type="hidden" name="gift" class="js-spin-val">
        <div class="modal__form-row">
            <input type="text" class="input-text" name="name" placeholder="Имя">
        </div>
        <div class="modal__form-row required">
            <input type="text" class="input-text js-masked" name="phone" placeholder="Телефон">
        </div>
        <div class="modal__form-row">
            <button type="submit" class="button button_full">Записаться</button>
        </div>
    </form>
</div>


</div>
<div width="0" height="0" style="overflow: hidden; position: absolute; width: 0; height: 0;">

    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <symbol viewBox="0 0 62 16" id="icon_arrow_callback" xmlns="http://www.w3.org/2000/svg">
            <path d="M60.707 8.707a1 1 0 000-1.414L54.343.929a1 1 0 00-1.414 1.414L58.586 8l-5.657 5.657a1 1 0 001.414 1.414l6.364-6.364zM0 9h60V7H0v2z"
            />
        </symbol>
        <symbol viewBox="0 0 9 15" id="icon_arrow_slider" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M5.108 7.5L.81 13.085l2.378 1.83L8.892 7.5 3.19.085.81 1.915 5.108 7.5z"
            />
        </symbol>
        <symbol viewBox="0 0 20 20" id="icon_close" xmlns="http://www.w3.org/2000/svg">
            <rect width="26.4" height="1.76" rx=".88" transform="scale(.99557 1.00441) rotate(45 .622 1.503)"
            />
            <rect width="26.4" height="1.76" rx=".88" transform="matrix(-.70397 .71023 .70397 .71023 18.76 0)"
            />
        </symbol>
        <symbol viewBox="0 0 15 14" id="icon_star" xmlns="http://www.w3.org/2000/svg">
            <path d="M7.5 0l2.204 4.466 4.929.716-3.566 3.477.841 4.909L7.5 11.25l-4.408 2.318.842-4.91L.367 5.183l4.929-.716L7.5 0z"
            />
        </symbol>
    </svg>
</div>
<svg width="0" height="0" style="overflow: hidden; position: absolute; width: 0; height: 0;">
    <!--width="97" viewBox="0 0 97 97" -->
    <symbol id=""></symbol>
    <symbol></symbol>
</svg>

<script src="./_assets/module/jquery/jquery.min.js"></script>
<script src="./_assets/module/fancybox/jquery.fancybox.min.js"></script>
<script src="./_assets/module/formstyler/jquery.formstyler.min.js"></script>
<script src="./_assets/module/owl-carousel/owl.carousel.min.js"></script>
<script src="./_assets/module/maskinput/jquery.inputmask.min.js"></script>
<script src="./_assets/js/bundle.min.js<?= $version; ?>"></script>

<!-- CallKeeper -->
<script async src="//callkeeper.ru/w/?398c534f"></script>

</body>

</html>

    <div class="policy">
        <div class="policy__image">
            <picture>
                <source srcset="./_assets/img/style/security_policy.jpg" media="(max-width: 770px)">
                <img src="./_assets/img/style/security_policy.jpg" alt="alt-img">
            </picture>
        </div>
    </div>
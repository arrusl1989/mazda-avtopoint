<header class="header">
    <div class="container">
        <div class="header__wrap">
            <div class="header__logo">
                <a href="#" class="logo">

                            <span class="logo__image">

                                <img src="./_assets/img/style/logo.svg" alt="">

                            </span>

                    <span class="logo__text">

                                <span class="logo__text-large">Аларм-Моторс</span>

                                <span class="logo__text-small">Сервисный центр Mazda</span>

                            </span>

                </a>
            </div>
            <div class="header__side"><a href="tel:+78124139242" class="header__phone">+7 812 413-92-42</a>
                <div
                        class="header__button"><a href="#service" data-fancybox class="button">

                        <span>записаться на сервис</span>

                    </a>
                </div>
            </div>
        </div>
    </div>
</header>
<section class="section">
    <div class="main-bg">
        <div class="main-bg__image">
            <a href="tel:+78124139242">
            <picture>
                <source srcset="./_assets/img/style/main_image-xs.jpg?v=3" media="(max-width: 500px)">
                <img src="./_assets/img/style/main_image.jpg?v=3" alt="alt-img">
            </picture>
            </a>
        </div>
    </div>
<!--    <div class="container">-->
<!--        <div class="main-info">-->
<!--            <div class="main-info__wrap">-->
<!--                <h1 class="main-info__title">Антибактериальная обработка кондиционера 800 <span class="rouble">₽</span>-->
<!--                    <span class="main-info__title-line">1950 <span class="rouble">₽</span></span></h1>-->
<!--                <div class="main-info__button"><a href="#service" data-fancybox class="button">-->
<!---->
<!--                        <span>Получить предложение</span>-->
<!---->
<!--                    </a>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
</section>
<section class="section section_pt section_pb">
    <div class="container">
        <h2 class="title-2">Акции</h2>
        <div class="actions">
            <div class="actions__slider js-actions-slider owl-carousel custom-slider">
                <div class="actions__item">
                    <div class="actions__image">
                        <div class="actions__image-wrap">
                            <picture>
                                <source srcset="./_assets/img/style/actions_image7-xs.jpg" media="(max-width: 770px)">
                                <img src="./_assets/img/style/actions_image7.jpg" alt="alt-img">
                            </picture>
                        </div>
                    </div>
                </div>
                <div class="actions__item">
                    <div class="actions__image">
                        <div class="actions__image-wrap">
                            <picture>
                                <source srcset="./_assets/img/style/actions_image8-xs.jpg" media="(max-width: 770px)">
                                <img src="./_assets/img/style/actions_image8.jpg" alt="alt-img">
                            </picture>
                        </div>
                    </div>
                </div>
                <div class="actions__item">
                    <div class="actions__image">
                        <div class="actions__image-wrap">
                            <picture>
                                <source srcset="./_assets/img/style/actions_image3-xs.jpg" media="(max-width: 770px)">
                                <img src="./_assets/img/style/actions_image3.jpg" alt="alt-img">
                            </picture>
                        </div>
                    </div>
                </div>
                <div class="actions__item">
                    <div class="actions__image">
                        <div class="actions__image-wrap">
                            <picture>
                                <source srcset="./_assets/img/style/actions_image9-xs.jpg" media="(max-width: 770px)">
                                <img src="./_assets/img/style/actions_image9.jpg" alt="alt-img">
                            </picture>
                        </div>
                    </div>
                </div>
                <div class="actions__item">
                    <div class="actions__image">
                        <div class="actions__image-wrap">
                            <picture>
                                <source srcset="./_assets/img/style/actions_image6-xs.jpg" media="(max-width: 770px)">
                                <img src="./_assets/img/style/actions_image6.jpg" alt="alt-img">
                            </picture>
                        </div>
                    </div>
                </div>
                <div class="actions__item">
                    <div class="actions__image">
                        <div class="actions__image-wrap">
                            <picture>
                                <source srcset="./_assets/img/style/actions_image10-xs.jpg" media="(max-width: 770px)">
                                <img src="./_assets/img/style/actions_image10.jpg" alt="alt-img">
                            </picture>
                        </div>
                    </div>
                </div>
                <div class="actions__item">
                    <div class="actions__image">
                        <div class="actions__image-wrap">
                            <picture>
                                <source srcset="./_assets/img/style/actions_image11-xs.jpg" media="(max-width: 770px)">
                                <img src="./_assets/img/style/actions_image11.jpg" alt="alt-img">
                            </picture>
                        </div>
                    </div>
                </div>
                <div class="actions__item">
                    <div class="actions__image">
                        <div class="actions__image-wrap">
                            <picture>
                                <source srcset="./_assets/img/style/actions_image12-xs.jpg" media="(max-width: 770px)">
                                <img src="./_assets/img/style/actions_image12.jpg" alt="alt-img">
                            </picture>
                        </div>
                    </div>
                </div>
                <div class="actions__item">
                    <div class="actions__image">
                        <div class="actions__image-wrap">
                            <picture>
                                <source srcset="./_assets/img/style/actions_image13-xs.jpg" media="(max-width: 770px)">
                                <img src="./_assets/img/style/actions_image13.jpg" alt="alt-img">
                            </picture>
                        </div>
                    </div>
                </div>
                <div class="actions__item">
                    <div class="actions__image">
                        <div class="actions__image-wrap">
                            <picture>
                                <source srcset="./_assets/img/style/actions_image14-xs.jpg" media="(max-width: 770px)">
                                <img src="./_assets/img/style/actions_image14.jpg" alt="alt-img">
                            </picture>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section section_pt section_pb">
    <div class="container">
        <h2 class="title-2">Стоимость ТО</h2>
        <div class="section__descr">Указана максимальная розничная цена без учета специальных предложений.
            Уточните в отделе сервиса персональную стомость ТО. Новым клиентам - выгода
            до 50%.
        </div>
        <div class="worth">
            <div class="worth__tabs">
                <div class="worth__tab js-worth-tab active" data-worth="1">Mazda 3</div>
                <div class="worth__tab js-worth-tab" data-worth="2">Mazda 6</div>
                <div class="worth__tab js-worth-tab" data-worth="3">Mazda CX-5</div>
                <div class="worth__tab js-worth-tab" data-worth="4">Mazda BT-50</div>
                <div class="worth__tab js-worth-tab" data-worth="5">Mazda CX-9</div>
            </div>
            <div class="worth__select">
                <select class="js-select js-worth-select">
                    <option value="1">Mazda 3</option>
                    <option value="2">Mazda 6</option>
                    <option value="3">Mazda CX-5</option>
                    <option value="4">Mazda BT-50</option>
                    <option value="5">Mazda CX-9</option>
                </select>
            </div>
            <div class="worth__content js-worth-wrap">
                <div class="worth__container js-worth-container active" data-worth="1">
                    <div class="worth__container-wrap">
                        <div class="worth__col worth__col_small">
                            <div class="worth__row worth__row_line">
                                <div class="worth__model">Mazda 3</div>
                                <div class="worth__year">2013 - 2019</div>
                            </div>
                            <div class="worth__row">
                                <div class="worth__model">1.5</div>
                            </div>
                            <div class="worth__row">
                                <div class="worth__model">1.6</div>
                            </div>
                            <div class="worth__row">
                                <div class="worth__model">2.0</div>
                            </div>
                        </div>
                        <div class="worth__col worth__col_large">
                            <div class="worth__table">
                                <table cellspacing="0">
                                    <tr>
                                        <th><span>ТО 1</span>(15&nbsp;000&nbsp;км)</th>
                                        <th><span>ТО 2</span>(30&nbsp;000&nbsp;км)</th>
                                        <th><span>ТО 3</span>(45&nbsp;000&nbsp;км)</th>
                                        <th><span>ТО 4</span>(60&nbsp;000&nbsp;км)</th>
                                        <th><span>ТО 5</span>(75&nbsp;000&nbsp;км)</th>
                                        <th><span>ТО 6</span>(90&nbsp;000&nbsp;км)</th>
                                    </tr>
                                    <tr>
                                        <td><span class="worth__price">11&nbsp;580&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">15&nbsp;180&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">13&nbsp;000&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">15&nbsp;830&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">11&nbsp;910&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">16&nbsp;920&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span class="worth__price">11&nbsp;060&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">16&nbsp;460&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">12&nbsp;610&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">16&nbsp;790&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">11&nbsp;390&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">18&nbsp;010&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span class="worth__price">11&nbsp;580&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">15&nbsp;180&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">13&nbsp;180&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">15&nbsp;830&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">11&nbsp;910&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">17&nbsp;100&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="worth__container-wrap">
                        <div class="worth__col worth__col_small">
                            <div class="worth__row worth__row_line">
                                <div class="worth__model">Mazda 3</div>
                                <div class="worth__year">2019 - н/в</div>
                            </div>
                            <div class="worth__row">
                                <div class="worth__model">1.5</div>
                            </div>
                            <div class="worth__row">
                                <div class="worth__model">2.0</div>
                            </div>
                        </div>
                        <div class="worth__col worth__col_large">
                            <div class="worth__table">
                                <table cellspacing="0">
                                    <tr>
                                        <th><span>ТО 1</span>(15&nbsp;000&nbsp;км)</th>
                                        <th><span>ТО 2</span>(30&nbsp;000&nbsp;км)</th>
                                        <th><span>ТО 3</span>(45&nbsp;000&nbsp;км)</th>
                                        <th><span>ТО 4</span>(60&nbsp;000&nbsp;км)</th>
                                        <th><span>ТО 5</span>(75&nbsp;000&nbsp;км)</th>
                                        <th><span>ТО 6</span>(90&nbsp;000&nbsp;км)</th>
                                    </tr>
                                    <tr>
                                        <td><span class="worth__price">12&nbsp;100&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">15&nbsp;800&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">14&nbsp;200&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">16&nbsp;500&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">12&nbsp;100&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">18&nbsp;200&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span class="worth__price">12&nbsp;100&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">15&nbsp;800&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">14&nbsp;700&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">16&nbsp;500&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">12&nbsp;100&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                        <td><span class="worth__price">18&nbsp;800&nbsp;<span
                                                        class="rouble">₽</span></span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="worth__content js-worth-wrap">
                    <div class="worth__container js-worth-container" data-worth="2">
                        <div class="worth__container-wrap">
                            <div class="worth__col worth__col_small">
                                <div class="worth__row worth__row_line">
                                    <div class="worth__model">Mazda 6</div>
                                    <div class="worth__year">2007 - 2012</div>
                                </div>
                                <div class="worth__row">
                                    <div class="worth__model">1.8</div>
                                </div>
                                <div class="worth__row">
                                    <div class="worth__model">2.0</div>
                                </div>
                                <div class="worth__row">
                                    <div class="worth__model">2.5</div>
                                </div>
                            </div>
                            <div class="worth__col worth__col_large">
                                <div class="worth__table">
                                    <table cellspacing="0">
                                        <tr>
                                            <th><span>ТО 1</span>(15&nbsp;000&nbsp;км)</th>
                                            <th><span>ТО 2</span>(30&nbsp;000&nbsp;км)</th>
                                            <th><span>ТО 3</span>(45&nbsp;000&nbsp;км)</th>
                                            <th>Выгода
                                                <br />сервис плюс
                                            </th>
                                            <th><span>ТО 4</span>(60&nbsp;000&nbsp;км)</th>
                                            <th><span>ТО 5</span>(75&nbsp;000&nbsp;км)</th>
                                            <th><span>ТО 6</span>(90&nbsp;000&nbsp;км)</th>
                                            <th>Выгода
                                                <br />сервис плюс
                                            </th>
                                        </tr>
                                        <tr>
                                            <td><span class="worth__price">7&nbsp;560&nbsp;<span class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">10&nbsp;470&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">8&nbsp;450&nbsp;<span class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price worth__price_color"><span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">10&nbsp;730&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">9&nbsp;330&nbsp;<span class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">11&nbsp;610&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price worth__price_color"><span
                                                            class="rouble">₽</span></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span class="worth__price">7&nbsp;670&nbsp;<span class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">10&nbsp;580&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">8&nbsp;560&nbsp;<span class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price worth__price_color"><span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">10&nbsp;830&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">14&nbsp;810&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">11&nbsp;720&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price worth__price_color"><span
                                                            class="rouble">₽</span></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span class="worth__price">7&nbsp;820&nbsp;<span class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">10&nbsp;720&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">8&nbsp;700&nbsp;<span class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price worth__price_color"><span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">10&nbsp;980&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">14&nbsp;960&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">11&nbsp;860&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price worth__price_color"><span
                                                            class="rouble">₽</span></span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="worth__container-wrap">
                            <div class="worth__col worth__col_small">
                                <div class="worth__row worth__row_line">
                                    <div class="worth__model">Mazda 6</div>
                                    <div class="worth__year">2012 - н/в</div>
                                </div>
                                <div class="worth__row">
                                    <div class="worth__model">2.0</div>
                                </div>
                                <div class="worth__row">
                                    <div class="worth__model">2.5</div>
                                </div>
                            </div>
                            <div class="worth__col worth__col_large">
                                <div class="worth__table">
                                    <table cellspacing="0">
                                        <tr>
                                            <th><span>ТО 1</span>(15&nbsp;000&nbsp;км)</th>
                                            <th><span>ТО 2</span>(30&nbsp;000&nbsp;км)</th>
                                            <th><span>ТО 3</span>(45&nbsp;000&nbsp;км)</th>
                                            <th>Выгода
                                                <br />сервис плюс
                                            </th>
                                            <th><span>ТО 4</span>(60&nbsp;000&nbsp;км)</th>
                                            <th><span>ТО 5</span>(75&nbsp;000&nbsp;км)</th>
                                            <th><span>ТО 6</span>(90&nbsp;000&nbsp;км)</th>
                                            <th>Выгода
                                                <br />сервис плюс
                                            </th>
                                        </tr>
                                        <tr>
                                            <td><span class="worth__price">11&nbsp;580&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">15&nbsp;180&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">13&nbsp;500&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price worth__price_color"><span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">15&nbsp;830&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">11&nbsp;910&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">17&nbsp;100&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price worth__price_color"><span
                                                            class="rouble">₽</span></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span class="worth__price">11&nbsp;980&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">15&nbsp;580&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">13&nbsp;900&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price worth__price_color"><span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">16&nbsp;230&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">12&nbsp;300&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">17&nbsp;500&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price worth__price_color"><span
                                                            class="rouble">₽</span></span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="worth__container-wrap">
                            <div class="worth__col worth__col_small">
                                <div class="worth__row worth__row_line">
                                    <div class="worth__model">Mazda 6</div>
                                    <div class="worth__year">2018 - н/в</div>
                                </div>
                                <div class="worth__row">
                                    <div class="worth__model">2.5T</div>
                                </div>
                            </div>
                            <div class="worth__col worth__col_large">
                                <div class="worth__table">
                                    <table cellspacing="0">
                                        <tr>
                                            <th><span>ТО 1</span>(15&nbsp;000&nbsp;км)</th>
                                            <th><span>ТО 2</span>(30&nbsp;000&nbsp;км)</th>
                                            <th><span>ТО 3</span>(45&nbsp;000&nbsp;км)</th>
                                            <th>Выгода
                                                <br />сервис плюс
                                            </th>
                                            <th><span>ТО 4</span>(60&nbsp;000&nbsp;км)</th>
                                            <th><span>ТО 5</span>(75&nbsp;000&nbsp;км)</th>
                                            <th><span>ТО 6</span>(90&nbsp;000&nbsp;км)</th>
                                            <th>Выгода
                                                <br />сервис плюс
                                            </th>
                                        </tr>
                                        <tr>
                                            <td><span class="worth__price">13&nbsp;040&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">16&nbsp;400&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">15&nbsp;540&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price worth__price_color"><span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">27&nbsp;590&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">13&nbsp;390&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price">18&nbsp;900&nbsp;<span
                                                            class="rouble">₽</span></span>
                                            </td>
                                            <td><span class="worth__price worth__price_color"><span
                                                            class="rouble">₽</span></span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="worth__content js-worth-wrap">
                        <div class="worth__container js-worth-container" data-worth="3">
                            <div class="worth__container-wrap">
                                <div class="worth__col worth__col_small">
                                    <div class="worth__row worth__row_line">
                                        <div class="worth__model">Mazda CX-5</div>
                                        <div class="worth__year">2012 - 2017</div>
                                    </div>
                                    <div class="worth__row">
                                        <div class="worth__model">2.0</div>
                                    </div>
                                    <div class="worth__row">
                                        <div class="worth__model">2.5</div>
                                    </div>
                                </div>
                                <div class="worth__col worth__col_large">
                                    <div class="worth__table">
                                        <table cellspacing="0">
                                            <tr>
                                                <th><span>ТО 1</span>(15&nbsp;000&nbsp;км)</th>
                                                <th><span>ТО 2</span>(30&nbsp;000&nbsp;км)</th>
                                                <th><span>ТО 3</span>(45&nbsp;000&nbsp;км)</th>
                                                <th>Выгода
                                                    <br />сервис плюс
                                                </th>
                                                <th><span>ТО 4</span>(60&nbsp;000&nbsp;км)</th>
                                                <th><span>ТО 5</span>(75&nbsp;000&nbsp;км)</th>
                                                <th><span>ТО 6</span>(90&nbsp;000&nbsp;км)</th>
                                                <th>Выгода
                                                    <br />сервис плюс
                                                </th>
                                            </tr>
                                            <tr>
                                                <td><span class="worth__price">12&nbsp;010&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">15&nbsp;370&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">13&nbsp;930&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price worth__price_color"><span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">15&nbsp;720&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">12&nbsp;360&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">17&nbsp;290&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price worth__price_color"><span
                                                                class="rouble">₽</span></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="worth__price">12&nbsp;400&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">15&nbsp;770&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">14&nbsp;320&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price worth__price_color"><span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">16&nbsp;470&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">12&nbsp;750&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">17&nbsp;690&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price worth__price_color"><span
                                                                class="rouble">₽</span></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="worth__container-wrap">
                                <div class="worth__col worth__col_small">
                                    <div class="worth__row worth__row_line">
                                        <div class="worth__model">Mazda CX-5</div>
                                        <div class="worth__year">2012 - 2017</div>
                                    </div>
                                    <div class="worth__row">
                                        <div class="worth__model">2.2D</div>
                                    </div>
                                </div>
                                <div class="worth__col worth__col_large">
                                    <div class="worth__table">
                                        <table cellspacing="0">
                                            <tr>
                                                <th><span>ТО 1</span>(10&nbsp;000&nbsp;км)</th>
                                                <th><span>ТО 2</span>(20&nbsp;000&nbsp;км)</th>
                                                <th><span>ТО 3</span>(30&nbsp;000&nbsp;км)</th>
                                                <th><span>ТО 4</span>(40&nbsp;000&nbsp;км)</th>
                                                <th><span>ТО 5</span>(50&nbsp;000&nbsp;км)</th>
                                                <th><span>ТО 6</span>(60&nbsp;000&nbsp;км)</th>
                                                <th><span>ТО 7</span>(70&nbsp;000&nbsp;км)</th>
                                                <th><span>ТО 8</span>(80&nbsp;000&nbsp;км)</th>
                                                <th><span>ТО 9</span>(90&nbsp;000&nbsp;км)</th>
                                                <th><span>ТО 10</span>(100&nbsp;000&nbsp;км)</th>
                                            </tr>
                                            <tr>
                                                <td><span class="worth__price">13&nbsp;090&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">16&nbsp;100&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">14&nbsp;250&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">16&nbsp;450&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">12&nbsp;390&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">30&nbsp;980&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">13&nbsp;090&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">16&nbsp;100&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">14&nbsp;250&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">17&nbsp;850&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="worth__container-wrap">
                                <div class="worth__col worth__col_small">
                                    <div class="worth__row worth__row_line">
                                        <div class="worth__model">Mazda CX-5</div>
                                        <div class="worth__year">2017 - н/в</div>
                                    </div>
                                    <div class="worth__row">
                                        <div class="worth__model">2.0</div>
                                    </div>
                                    <div class="worth__row">
                                        <div class="worth__model">2.5</div>
                                    </div>
                                </div>
                                <div class="worth__col worth__col_large">
                                    <div class="worth__table">
                                        <table cellspacing="0">
                                            <tr>
                                                <th><span>ТО 1</span>(15&nbsp;000&nbsp;км)</th>
                                                <th><span>ТО 2</span>(30&nbsp;000&nbsp;км)</th>
                                                <th><span>ТО 3</span>(45&nbsp;000&nbsp;км)</th>
                                                <th>Выгода
                                                    <br />сервис плюс
                                                </th>
                                                <th><span>ТО 4</span>(60&nbsp;000&nbsp;км)</th>
                                                <th><span>ТО 5</span>(75&nbsp;000&nbsp;км)</th>
                                                <th><span>ТО 6</span>(90&nbsp;000&nbsp;км)</th>
                                                <th>Выгода
                                                    <br />сервис плюс
                                                </th>
                                            </tr>
                                            <tr>
                                                <td><span class="worth__price">12&nbsp;010&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">15&nbsp;370&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">13&nbsp;930&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price worth__price_color"><span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">15&nbsp;720&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">12&nbsp;360&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">17&nbsp;290&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price worth__price_color"><span
                                                                class="rouble">₽</span></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="worth__price">12&nbsp;400&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">15&nbsp;770&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">14&nbsp;320&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price worth__price_color"><span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">16&nbsp;470&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">12&nbsp;750&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price">17&nbsp;690&nbsp;<span
                                                                class="rouble">₽</span></span>
                                                </td>
                                                <td><span class="worth__price worth__price_color"><span
                                                                class="rouble">₽</span></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="worth__content js-worth-wrap">
                            <div class="worth__container js-worth-container" data-worth="4">
                                <div class="worth__container-wrap">
                                    <div class="worth__col worth__col_small">
                                        <div class="worth__row worth__row_line">
                                            <div class="worth__model">Mazda BT-50</div>
                                            <div class="worth__year">2008 - 2011</div>
                                        </div>
                                        <div class="worth__row">
                                            <div class="worth__model">2.5</div>
                                        </div>
                                    </div>
                                    <div class="worth__col worth__col_large">
                                        <div class="worth__table">
                                            <table cellspacing="0">
                                                <tr>
                                                    <th><span>ТО 1</span>(10&nbsp;000&nbsp;км)</th>
                                                    <th><span>ТО 2</span>(15&nbsp;000&nbsp;км)</th>
                                                    <th><span>ТО 3</span>(20&nbsp;000&nbsp;км)</th>
                                                    <th><span>ТО 4</span>(30&nbsp;000&nbsp;км)</th>
                                                    <th><span>ТО 5</span>(40&nbsp;000&nbsp;км)</th>
                                                    <th><span>ТО 6</span>(45&nbsp;000&nbsp;км)</th>
                                                    <th><span>ТО 7</span>(50&nbsp;000&nbsp;км)</th>
                                                    <th><span>ТО 8</span>(60&nbsp;000&nbsp;км)</th>
                                                    <th><span>ТО 9</span>(70&nbsp;000&nbsp;км)</th>
                                                    <th><span>ТО 10</span>(75&nbsp;000&nbsp;км)</th>
                                                    <th><span>ТО 11</span>(80&nbsp;000&nbsp;км)</th>
                                                    <th><span>ТО 12</span>(90&nbsp;000&nbsp;км)</th>
                                                </tr>
                                                <tr>
                                                    <td><span class="worth__price">8&nbsp;930&nbsp;<span class="rouble">₽</span></span>
                                                    </td>
                                                    <td><span class="worth__price">14&nbsp;120&nbsp;<span
                                                                    class="rouble">₽</span></span>
                                                    </td>
                                                    <td><span class="worth__price">8&nbsp;930&nbsp;<span class="rouble">₽</span></span>
                                                    </td>
                                                    <td><span class="worth__price">15&nbsp;500&nbsp;<span
                                                                    class="rouble">₽</span></span>
                                                    </td>
                                                    <td><span class="worth__price">8&nbsp;930&nbsp;<span class="rouble">₽</span></span>
                                                    </td>
                                                    <td><span class="worth__price">17&nbsp;720&nbsp;<span
                                                                    class="rouble">₽</span></span>
                                                    </td>
                                                    <td><span class="worth__price">8&nbsp;930&nbsp;<span class="rouble">₽</span></span>
                                                    </td>
                                                    <td><span class="worth__price">16&nbsp;910&nbsp;<span
                                                                    class="rouble">₽</span></span>
                                                    </td>
                                                    <td><span class="worth__price">8&nbsp;930&nbsp;<span class="rouble">₽</span></span>
                                                    </td>
                                                    <td><span class="worth__price">19&nbsp;570&nbsp;<span
                                                                    class="rouble">₽</span></span>
                                                    </td>
                                                    <td><span class="worth__price">8&nbsp;930&nbsp;<span class="rouble">₽</span></span>
                                                    </td>
                                                    <td><span class="worth__price">20&nbsp;520&nbsp;<span
                                                                    class="rouble">₽</span></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="worth__content js-worth-wrap">
                                <div class="worth__container js-worth-container" data-worth="5">
                                    <div class="worth__container-wrap">
                                        <div class="worth__col worth__col_small">
                                            <div class="worth__row worth__row_line">
                                                <div class="worth__model">Mazda CX-9</div>
                                                <div class="worth__year">2012 - 2014</div>
                                            </div>
                                            <div class="worth__row">
                                                <div class="worth__model">3.7</div>
                                            </div>
                                        </div>
                                        <div class="worth__col worth__col_large">
                                            <div class="worth__table">
                                                <table cellspacing="0">
                                                    <tr>
                                                        <th><span>ТО 1</span>(15&nbsp;000&nbsp;км)</th>
                                                        <th><span>ТО 2</span>(30&nbsp;000&nbsp;км)</th>
                                                        <th><span>ТО 3</span>(45&nbsp;000&nbsp;км)</th>
                                                        <th>Выгода
                                                            <br />сервис плюс
                                                        </th>
                                                        <th><span>ТО 4</span>(60&nbsp;000&nbsp;км)</th>
                                                        <th><span>ТО 5</span>(75&nbsp;000&nbsp;км)</th>
                                                        <th><span>ТО 6</span>(90&nbsp;000&nbsp;км)</th>
                                                        <th>Выгода
                                                            <br />сервис плюс
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td><span class="worth__price">14&nbsp;140&nbsp;<span
                                                                        class="rouble">₽</span></span>
                                                        </td>
                                                        <td><span class="worth__price">18&nbsp;930&nbsp;<span
                                                                        class="rouble">₽</span></span>
                                                        </td>
                                                        <td><span class="worth__price">16&nbsp;240&nbsp;<span
                                                                        class="rouble">₽</span></span>
                                                        </td>
                                                        <td><span class="worth__price worth__price_color"><span
                                                                        class="rouble">₽</span></span>
                                                        </td>
                                                        <td><span class="worth__price">19&nbsp;600&nbsp;<span
                                                                        class="rouble">₽</span></span>
                                                        </td>
                                                        <td><span class="worth__price">25&nbsp;510&nbsp;<span
                                                                        class="rouble">₽</span></span>
                                                        </td>
                                                        <td><span class="worth__price">21&nbsp;030&nbsp;<span
                                                                        class="rouble">₽</span></span>
                                                        </td>
                                                        <td><span class="worth__price worth__price_color"><span
                                                                        class="rouble">₽</span></span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="worth__container-wrap">
                                        <div class="worth__col worth__col_small">
                                            <div class="worth__row worth__row_line">
                                                <div class="worth__model">Mazda CX-9</div>
                                                <div class="worth__year">2017 - н/в</div>
                                            </div>
                                            <div class="worth__row">
                                                <div class="worth__model">2.5</div>
                                            </div>
                                        </div>
                                        <div class="worth__col worth__col_large">
                                            <div class="worth__table">
                                                <table cellspacing="0">
                                                    <tr>
                                                        <th><span>ТО 1</span>(15&nbsp;000&nbsp;км)</th>
                                                        <th><span>ТО 2</span>(30&nbsp;000&nbsp;км)</th>
                                                        <th><span>ТО 3</span>(45&nbsp;000&nbsp;км)</th>
                                                        <th>Выгода
                                                            <br />сервис плюс
                                                        </th>
                                                        <th><span>ТО 4</span>(60&nbsp;000&nbsp;км)</th>
                                                        <th><span>ТО 5</span>(75&nbsp;000&nbsp;км)</th>
                                                        <th><span>ТО 6</span>(90&nbsp;000&nbsp;км)</th>
                                                        <th>Выгода
                                                            <br />сервис плюс
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td><span class="worth__price">14&nbsp;330&nbsp;<span
                                                                        class="rouble">₽</span></span>
                                                        </td>
                                                        <td><span class="worth__price">18&nbsp;050&nbsp;<span
                                                                        class="rouble">₽</span></span>
                                                        </td>
                                                        <td><span class="worth__price">15&nbsp;780&nbsp;<span
                                                                        class="rouble">₽</span></span>
                                                        </td>
                                                        <td><span class="worth__price worth__price_color"><span
                                                                        class="rouble">₽</span></span>
                                                        </td>
                                                        <td><span class="worth__price">29&nbsp;580&nbsp;<span
                                                                        class="rouble">₽</span></span>
                                                        </td>
                                                        <td><span class="worth__price">14&nbsp;330&nbsp;<span
                                                                        class="rouble">₽</span></span>
                                                        </td>
                                                        <td><span class="worth__price">20&nbsp;550&nbsp;<span
                                                                        class="rouble">₽</span></span>
                                                        </td>
                                                        <td><span class="worth__price worth__price_color"><span
                                                                        class="rouble">₽</span></span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="worth__button">
                                <a href="#service" data-fancybox class="button"><span>Рассчитать стоимость то</span></a>
                            </div>
                        </div>
</section>
<section class="section section_pt section_pb">
    <div class="container">
        <h2 class="title-2">Услуги по слесарному ремонту</h2>
        <form action="#" class="services">
            <div class="services__content">
                <h3 class="services__title">Выберите группу ремонта</h3>
                <div class="services__container services__container_static js-services-slider custom-slider">
                    <label class="services__radio">
                        <input type="radio" name="services" class="js-services" data-services="1"
                               checked> <span class="services__radio-wrap">

                                    <span class="services__radio-image">

                                        <img src="./_assets/img/style/icons/icon_services1.svg" alt="">

                                    </span>
 <span class="services__radio-title">Популярные услуги</span>
</span>
                    </label>
                    <label class="services__radio">
                        <input type="radio" name="services" class="js-services" data-services="2"> <span
                                class="services__radio-wrap">

                                    <span class="services__radio-image">

                                        <img src="./_assets/img/style/icons/icon_services2.svg" alt="">

                                    </span>
 <span class="services__radio-title">Ремонт подвески</span>
</span>
                    </label>
                    <label class="services__radio">
                        <input type="radio" name="services" class="js-services" data-services="3"> <span
                                class="services__radio-wrap">

                                    <span class="services__radio-image">

                                        <img src="./_assets/img/style/icons/icon_services3.svg" alt="">

                                    </span>
 <span class="services__radio-title">Диагностика</span>
</span>
                    </label>
                    <label class="services__radio">
                        <input type="radio" name="services" class="js-services" data-services="4"> <span
                                class="services__radio-wrap">

                                    <span class="services__radio-image">

                                        <img src="./_assets/img/style/icons/icon_services4.svg" alt="">

                                    </span>
 <span class="services__radio-title">Ремонт тормозной системы</span>
</span>
                    </label>
                    <label class="services__radio">
                        <input type="radio" name="services" class="js-services" data-services="5"> <span
                                class="services__radio-wrap">

                                    <span class="services__radio-image">

                                        <img src="./_assets/img/style/icons/icon_services5.svg" alt="">

                                    </span>
 <span class="services__radio-title">Ремонт электрооборудования</span>
</span>
                    </label>
                    <label class="services__radio">
                        <input type="radio" name="services" class="js-services" data-services="6"> <span
                                class="services__radio-wrap">

                                    <span class="services__radio-image">

                                        <img src="./_assets/img/style/icons/icon_services6.svg" alt="">

                                    </span>
 <span class="services__radio-title">Шиномонтаж и развал-схождение</span>
</span>
                    </label>
                </div>
            </div>
            <h3 class="services__title">Выберите услугу</h3>
            <div class="services__content js-services-wrap">
                <div class="services__container active js-services-container js-services-slider custom-slider"
                     data-services="1">
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Замена масла в двигателе и масл.фильтра"
                               data-price="750"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Замена масла в двигателе и масл.фильтра</span>
                                <span
                                        class="services__checkbox-price">750 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Замена топливного фильтра (в топливном баке)"
                               data-price="1500"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Замена топливного фильтра (в топливном баке)</span>
                                <span
                                        class="services__checkbox-price">1 500 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check" value="Замена масла в АКПП"
                               data-price="1200"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Замена масла в АКПП</span>
                                <span
                                        class="services__checkbox-price">1 200 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Замена салонного фильтра"
                               data-price="300"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Замена салонного фильтра</span>
                                <span
                                        class="services__checkbox-price">300 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Замена воздушного фильтра двигателя"
                               data-price="300"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Замена воздушного фильтра двигателя</span>
                                <span
                                        class="services__checkbox-price">300 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Антибактериальная обработка кондиционера"
                               data-price="800"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Антибактериальная обработка кондиционера</span>
                                <span
                                        class="services__checkbox-price">800 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check" value="Мойка моторного отсека"
                               data-price="750"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Мойка моторного отсека</span>
                                <span
                                        class="services__checkbox-price">750 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Замена свечей зажигания (4 ЦЛ Двигатель)"
                               data-price="600"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Замена свечей зажигания (4 ЦЛ Двигатель)</span>
                                <span
                                        class="services__checkbox-price">600 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check" value="Замена приводного ремня"
                               data-price="600"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Замена приводного ремня</span>
                                <span
                                        class="services__checkbox-price">600 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Замена щеток стеклоочистителя"
                               data-price="300"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Замена щеток стеклоочистителя</span>
                                <span
                                        class="services__checkbox-price">300 ₽</span>
                                    </span>
                    </label>
                </div>

                <div class="services__container js-services-container js-services-slider custom-slider"
                     data-services="2">
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Замена сайлент-блоков (перепресовка)"
                               data-price="750"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Замена сайлент-блоков (перепресовка)</span>
                                <span
                                        class="services__checkbox-price">750 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Замена стоек стабилизатора (2 шт.)"
                               data-price="750"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Замена стоек стабилизатора (2 шт.)</span>
                                <span
                                        class="services__checkbox-price">750 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Замена втулок стабилизатора (2 шт.)"
                               data-price="750"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Замена втулок стабилизатора (2 шт.)</span>
                                <span
                                        class="services__checkbox-price">750 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Замена переднего амортизатора"
                               data-price="1500"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Замена переднего амортизатора</span>
                                <span
                                        class="services__checkbox-price">1500 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Замена заднего амотризатора"
                               data-price="1500"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Замена заднего амотризатора</span>
                                <span
                                        class="services__checkbox-price">1500 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Замена пружин передней подвески (2 шт.)"
                               data-price="3750"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Замена пружин передней подвески (2 шт.)</span>
                                <span
                                        class="services__checkbox-price">3750 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Замена пружин задней подвески (2 шт.)"
                               data-price="3000"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Замена пружин задней подвески (2 шт.)</span>
                                <span
                                        class="services__checkbox-price">3000 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Замена подшипника ступицы"
                               data-price="1500"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Замена подшипника ступицы</span>
                                <span
                                        class="services__checkbox-price">1500 ₽</span>
                                    </span>
                    </label>
                </div>

                <div class="services__container js-services-container js-services-slider custom-slider"
                     data-services="3">
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Контрольный осмотр тормозной системы"
                               data-price="750"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Контрольный осмотр тормозной системы</span>
                                <span
                                        class="services__checkbox-price">750 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Контрольный осмотр ходовой части"
                               data-price="750"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Контрольный осмотр ходовой части</span>
                                <span
                                        class="services__checkbox-price">750 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Диагностика ходовой/тормозной системы на стенде MAHA"
                               data-price="1500"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Диагностика ходовой/тормозной системы на стенде MAHA</span>
                                <span
                                        class="services__checkbox-price">1500 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Компьютерная диагностика сканером"
                               data-price="1500"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Компьютерная диагностика сканером</span>
                                <span
                                        class="services__checkbox-price">1500 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Диагностика и перезаправка кондиционера"
                               data-price="2500"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Диагностика и перезаправка кондиционера</span>
                                <span
                                        class="services__checkbox-price">2500 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Проверка углов установки колес (проверка развала)"
                               data-price="1"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Проверка углов установки колес (проверка развала)</span>
                                <span
                                        class="services__checkbox-price">1 ₽</span>
                                    </span>
                    </label>
                </div>

                <div class="services__container js-services-container js-services-slider custom-slider"
                     data-services="4">
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Замена тормозных колодок"
                               data-price="750"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Замена тормозных колодок</span>
                                <span
                                        class="services__checkbox-price">750 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check" value="Замена тормозных дисков"
                               data-price="750"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Замена тормозных дисков</span>
                                <span
                                        class="services__checkbox-price">750 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Замена тормозной жидкости"
                               data-price="750"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Замена тормозной жидкости</span>
                                <span
                                        class="services__checkbox-price">750 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Регулировка стояночного тормоза"
                               data-price="750"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Регулировка стояночного тормоза</span>
                                <span
                                        class="services__checkbox-price">750 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Профилактика тормозных механизмов "
                               data-price="1200"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Профилактика тормозных механизмов </span>
                                <span
                                        class="services__checkbox-price">1200 ₽</span>
                                    </span>
                    </label>
                </div>

                <div class="services__container js-services-container js-services-slider custom-slider"
                     data-services="5">
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Замена лампы накаливания фары"
                               data-price="300"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Замена лампы накаливания фары</span>
                                <span
                                        class="services__checkbox-price">300 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Замена лампы накаливания ПТФ"
                               data-price="300"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Замена лампы накаливания ПТФ</span>
                                <span
                                        class="services__checkbox-price">300 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check" value="Замена предохранителя"
                               data-price="300"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Замена предохранителя</span>
                                <span
                                        class="services__checkbox-price">300 ₽</span>
                                    </span>
                    </label>
                </div>

                <div class="services__container js-services-container js-services-slider custom-slider"
                     data-services="6">
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Шиномонтаж и балансировка легкового автомбиля 4 колеса"
                               data-price="2500"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Шиномонтаж и балансировка легкового автомбиля 4 колеса</span>
                                <span
                                        class="services__checkbox-price">2500 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Балансировка одного колеса"
                               data-price="375"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Балансировка одного колеса</span>
                                <span
                                        class="services__checkbox-price">375 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Регулировка углов установки колес / развал-схождение"
                               data-price="2000"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Регулировка углов установки колес / развал-схождение</span>
                                <span
                                        class="services__checkbox-price">2000 ₽</span>
                                    </span>
                    </label>
                    <label class="services__checkbox">
                        <input type="checkbox" name="services" class="js-services-check"
                               value="Сезонное хранение колес (на сезон)"
                               data-price="2599"> <span class="services__checkbox-wrap">

                                    <span class="services__checkbox-title">Сезонное хранение колес (на сезон)</span>
                                <span
                                        class="services__checkbox-price">2599 ₽</span>
                                    </span>
                    </label>
                </div>
            </div>
            <div class="services__total">Итого: <span><span class="js-services-total">0</span>  <span
                            class="rouble">₽</span></span>
            </div>
            <div class="services__button" data-fancybox data-src="#service_order"><a href="" class="button"><span>записаться на сервис</span></a>
            </div>
            <div class="services__warn">* Стоимость включает в себя только стоимость работ, запчасти подбираются
                по вин-номеру, и на них также действуют хорошие скидки!
            </div>
        </form>
    </div>
</section>
<section class="section section_pt section_pb">
    <div class="container">
        <h2 class="title-2">Наши мастера</h2>
        <div class="masters">
            <div class="masters__slider js-masters-slider owl-carousel custom-slider custom-slider_one">
                <div class="masters__item">
                    <div class="masters__row">
                        <div class="masters__image">
                            <div class="masters__image-wrap">
                                <picture>
                                    <source srcset="./_assets/img/content/masters_image1-xs.jpg" media="(max-width: 770px)">
                                    <img src="./_assets/img/content/masters_image1.jpg" alt="alt-img">
                                </picture>
                            </div>
                        </div>
                        <div class="masters__info">
                            <div class="masters__name">Олег Калганов</div>
                            <div class="masters__job">Диагност – электрик</div>
                            <div class="masters__rating">
                                <div class="rating">
                                    <div class="rating__item active">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                    <div class="rating__item active">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                    <div class="rating__item active">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                    <div class="rating__item active">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                    <div class="rating__item">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="masters__row masters__row_fs">
                        <div class="masters__data">
                            <div class="masters__data-num">10 лет</div>
                            <div class="masters__data-descr">Опыт работы</div>
                        </div>
                        <div class="masters__data">
                            <div class="masters__data-num">8 лет</div>
                            <div class="masters__data-descr">Опыт ремонта и
                                <br />обслуживания Мазда
                            </div>
                        </div>
                    </div>
                    <div class="masters__button"><a href="#master_order" data-fancybox data-name="Олег Калганов"
                                                    class="button button_full js-master-name"><span>записаться к мастеру</span></a>
                    </div>
                    <div class="masters__more"><a href="#master1" data-fancybox class="masters__more-btn">Подробнее</a>
                    </div>
                </div>
                <div class="masters__item">
                    <div class="masters__row">
                        <div class="masters__image">
                            <div class="masters__image-wrap">
                                <picture>
                                    <source srcset="./_assets/img/content/masters_image2-xs.jpg" media="(max-width: 770px)">
                                    <img src="./_assets/img/content/masters_image2.jpg" alt="alt-img">
                                </picture>
                            </div>
                        </div>
                        <div class="masters__info">
                            <div class="masters__name">Мстислав Нефляшев</div>
                            <div class="masters__job">Автомеханик-агрегатчик</div>
                            <div class="masters__rating">
                                <div class="rating">
                                    <div class="rating__item active">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                    <div class="rating__item active">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                    <div class="rating__item active">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                    <div class="rating__item active">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                    <div class="rating__item">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="masters__row masters__row_fs">
                        <div class="masters__data">
                            <div class="masters__data-num">7 лет</div>
                            <div class="masters__data-descr">Опыт работы</div>
                        </div>
                        <div class="masters__data">
                            <div class="masters__data-num">7 лет</div>
                            <div class="masters__data-descr">Опыт ремонта и
                                <br />обслуживания Мазда
                            </div>
                        </div>
                    </div>
                    <div class="masters__button"><a href="#master_order" data-fancybox data-name="Мстислав Нефляшев"
                                                    class="button button_full js-master-name"><span>записаться к мастеру</span></a>
                    </div>
                    <div class="masters__more"><a href="#master2" data-fancybox class="masters__more-btn">Подробнее</a>
                    </div>
                </div>
                <div class="masters__item">
                    <div class="masters__row">
                        <div class="masters__image">
                            <div class="masters__image-wrap">
                                <picture>
                                    <source srcset="./_assets/img/content/masters_image3-xs.jpg" media="(max-width: 770px)">
                                    <img src="./_assets/img/content/masters_image3.jpg" alt="alt-img">
                                </picture>
                            </div>
                        </div>
                        <div class="masters__info">
                            <div class="masters__name">Юрий Васильев</div>
                            <div class="masters__job">Автомеханик-диагност</div>
                            <div class="masters__rating">
                                <div class="rating">
                                    <div class="rating__item active">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                    <div class="rating__item active">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                    <div class="rating__item active">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                    <div class="rating__item active">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                    <div class="rating__item">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="masters__row masters__row_fs">
                        <div class="masters__data">
                            <div class="masters__data-num">22 года</div>
                            <div class="masters__data-descr">Опыт работы</div>
                        </div>
                        <div class="masters__data">
                            <div class="masters__data-num">13 лет</div>
                            <div class="masters__data-descr">Опыт ремонта и
                                <br />обслуживания Мазда
                            </div>
                        </div>
                    </div>
                    <div class="masters__button"><a href="#master_order" data-fancybox data-name="Юрий Васильев"
                                                    class="button button_full js-master-name"><span>записаться к мастеру</span></a>
                    </div>
                    <div class="masters__more"><a href="#master3" data-fancybox class="masters__more-btn">Подробнее</a>
                    </div>
                </div>
                <div class="masters__item">
                    <div class="masters__row">
                        <div class="masters__image">
                            <div class="masters__image-wrap">
                                <picture>
                                    <source srcset="./_assets/img/content/masters_image4-xs.jpg" media="(max-width: 770px)">
                                    <img src="./_assets/img/content/masters_image4.jpg" alt="alt-img">
                                </picture>
                            </div>
                        </div>
                        <div class="masters__info">
                            <div class="masters__name">Сергей Шмелев</div>
                            <div class="masters__job">Диагност – электрик</div>
                            <div class="masters__rating">
                                <div class="rating">
                                    <div class="rating__item active">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                    <div class="rating__item active">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                    <div class="rating__item active">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                    <div class="rating__item active">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                    <div class="rating__item">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="masters__row masters__row_fs">
                        <div class="masters__data">
                            <div class="masters__data-num">21 год</div>
                            <div class="masters__data-descr">Опыт работы</div>
                        </div>
                        <div class="masters__data">
                            <div class="masters__data-num">12 лет</div>
                            <div class="masters__data-descr">Опыт ремонта и
                                <br />обслуживания Мазда
                            </div>
                        </div>
                    </div>
                    <div class="masters__button"><a href="#master_order" data-fancybox data-name="Олег Калганов"
                                                    class="button button_full js-master-name"><span>записаться к мастеру</span></a>
                    </div>
                    <div class="masters__more"><a href="#master4" data-fancybox class="masters__more-btn">Подробнее</a>
                    </div>
                </div>
                <div class="masters__item">
                    <div class="masters__row">
                        <div class="masters__image">
                            <div class="masters__image-wrap">
                                <picture>
                                    <source srcset="./_assets/img/content/masters_image5-xs.jpg" media="(max-width: 770px)">
                                    <img src="./_assets/img/content/masters_image5.jpg" alt="alt-img">
                                </picture>
                            </div>
                        </div>
                        <div class="masters__info">
                            <div class="masters__name">Алексей Рясянен</div>
                            <div class="masters__job">Автомеханик-агрегатчик</div>
                            <div class="masters__rating">
                                <div class="rating">
                                    <div class="rating__item active">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                    <div class="rating__item active">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                    <div class="rating__item active">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                    <div class="rating__item active">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                    <div class="rating__item">
                                        <svg>
                                            <use xlink:href="#icon_star" />
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="masters__row masters__row_fs">
                        <div class="masters__data">
                            <div class="masters__data-num">20 лет</div>
                            <div class="masters__data-descr">Опыт работы</div>
                        </div>
                        <div class="masters__data">
                            <div class="masters__data-num">12 лет</div>
                            <div class="masters__data-descr">Опыт ремонта и
                                <br />обслуживания Мазда
                            </div>
                        </div>
                    </div>
                    <div class="masters__button"><a href="#master_order" data-fancybox data-name="Юрий Васильев"
                                                    class="button button_full js-master-name"><span>записаться к мастеру</span></a>
                    </div>
                    <div class="masters__more"><a href="#master5" data-fancybox class="masters__more-btn">Подробнее</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section section_pt section_pb">
    <div class="container">
        <h2 class="title-2">Испытайте удачу</h2>
        <div class="section__descr">Получите случайный подарок при записи на сервис</div>
        <div class="spinwheel">
            <!--spinwheel__item нужно зациклить чтобы было 56-->
            <div class="spinwheel__slider js-spinwheel owl-carousel">
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel1.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Антидождь</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel2.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Контроль тормозов</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel3.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Мойка
                                <br />моторного отсека
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel4.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Антибактериальная
                                <br />обработка
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel5.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Диагностика авто</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel6.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Проверка развал
                                <br />/схождения
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel7.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Замена
                                <br />тормозных колодок
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel1.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Антидождь</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel2.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Контроль тормозов</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel3.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Мойка
                                <br />моторного отсека
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel4.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Антибактериальная
                                <br />обработка
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel5.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Диагностика авто</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel6.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Проверка развал
                                <br />/схождения
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel7.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Замена
                                <br />тормозных колодок
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel1.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Антидождь</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel2.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Контроль тормозов</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel3.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Мойка
                                <br />моторного отсека
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel4.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Антибактериальная
                                <br />обработка
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel5.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Диагностика авто</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel6.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Проверка развал
                                <br />/схождения
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel7.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Замена
                                <br />тормозных колодок
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel1.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Антидождь</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel2.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Контроль тормозов</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel3.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Мойка
                                <br />моторного отсека
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel4.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Антибактериальная
                                <br />обработка
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel5.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Диагностика авто</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel6.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Проверка развал
                                <br />/схождения
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel7.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Замена
                                <br />тормозных колодок
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel1.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Антидождь</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel2.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Контроль тормозов</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel3.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Мойка
                                <br />моторного отсека
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel4.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Антибактериальная
                                <br />обработка
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel5.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Диагностика авто</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel6.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Проверка развал
                                <br />/схождения
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel7.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Замена
                                <br />тормозных колодок
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel1.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Антидождь</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel2.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Контроль тормозов</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel3.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Мойка
                                <br />моторного отсека
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel4.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Антибактериальная
                                <br />обработка
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel5.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Диагностика авто</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel6.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Проверка развал
                                <br />/схождения
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel7.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Замена
                                <br />тормозных колодок
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel1.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Антидождь</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel2.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Контроль тормозов</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel3.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Мойка
                                <br />моторного отсека
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel4.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Антибактериальная
                                <br />обработка
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel5.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Диагностика авто</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel6.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Проверка развал
                                <br />/схождения
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel7.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Замена
                                <br />тормозных колодок
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel1.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Антидождь</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel2.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Контроль тормозов</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel3.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Мойка
                                <br />моторного отсека
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel4.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Антибактериальная
                                <br />обработка
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel5.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Диагностика авто</div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel6.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Проверка развал
                                <br />/схождения
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spinwheel__item">
                    <div class="spinwheel__wrap">
                        <div class="spinwheel__wrap-inner">
                            <div class="spinwheel__image">
                                <img src="./_assets/img/style/icons/icon_spinwheel7.svg" alt="">
                            </div>
                            <div class="spinwheel__title">Замена
                                <br />тормозных колодок
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="spinwheel__button">
                <div class="button js-spin-btn"><span>Испытать удачу</span>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section section_pt section_pb checkprice__bg">
    <div class="container">
        <h2 class="title-2">Узнайте стоимость ремонта по фото</h2>
        <div class="section__descr">Загрузите фотографию автомобиля и отправьте заявку, мы предварительно
            сделаем оценку стоимомсти ремонта по фото.
        </div>
        <form action="" class="checkprice js-ajax-form" enctype="multipart/form-data">
            <input type="hidden" name="action" value="assesment">
            <div class="checkprice__photos js-photos-wrap required">
                <input type="file" name="file[]" accept=".jpg, .jpeg, .png" class="input-file js-upload"
                       multiple="multiple">
            </div>
            <div class="checkprice__inputs">
                <div class="checkprice__input">
                    <input type="text" class="input-text" name="name" placeholder="Имя">
                </div>
                <div class="checkprice__input required">
                    <input type="text" class="input-text js-masked" name="phone" placeholder="Телефон">
                </div>
                <div class="checkprice__button">
                    <button type="submit" class="button">Узнать стоимость</button>
                </div>
            </div>
            <div class="checkprice__warn">Нажав кнопку "Узнать стоимость", я даю <a href="#">согласие на обработку моих
                    персональных данных и получение рекламы</a>.
            </div>
        </form>
    </div>
</section>
<section class="section section_pt section_pb">
    <div class="container">
        <div class="gallery">
            <div class="gallery__col gallery__col_small">
                <h2 class="title-2 gallery__title">Гарантийный сервис-лоукостер Аларм-Моторс</h2>
                <div class="gallery__descr">
                    <p>Аларм-Моторс – первый официальный сервисный центр Mazda в Санкт-Петербурге,
                        который предлагает всем клиентам единую низкую стоимость работ вне зависимости
                        от возраста автомобиля и гарантирует неизменное качество официального дилера.</p>
                    <p>Доверяя заботу об автомобиле официальному дилеру, вы можете быть абсолютно
                        уверены в том, что к вашим услугам высочайший уровень клиентоориентированности
                        и бескомпромиссное качество по привлекательным ценам.</p>
                </div>
            </div>
            <div class="gallery__col gallery__col_large">
                <div class="card-slider custom-slider custom-slider_one">
                    <div class="card-slider__button card-slider__button_prev js-card-slider-prev">
                        <svg class="icon icon_arrow-right">
                            <use xlink:href="#icon_arrow_slider" />
                        </svg>
                    </div>
                    <div class="card-slider__button card-slider__button_next js-card-slider-next">
                        <svg class="icon icon_arrow-right">
                            <use xlink:href="#icon_arrow_slider" />
                        </svg>
                    </div>
                    <div class="card-slider__image">
                        <img src="./_assets/img/content/card-slider4.jpg" alt="">
                    </div>
                    <div class="card-slider__items js-kid-slider">
                        <div class="card-slider__item prev">
                            <picture>
                                <source srcset="./_assets/img/content/card-slider4-xs.jpg" media="(max-width: 770px)">
                                <img src="./_assets/img/content/card-slider4.jpg" alt="alt-img">
                            </picture>
                        </div>
                        <div class="card-slider__item active">
                            <picture>
                                <source srcset="./_assets/img/content/card-slider2-xs.jpg" media="(max-width: 770px)">
                                <img src="./_assets/img/content/card-slider2.jpg" alt="alt-img">
                            </picture>
                        </div>
                        <div class="card-slider__item next">
                            <picture>
                                <source srcset="./_assets/img/content/card-slider3-xs.jpg" media="(max-width: 770px)">
                                <img src="./_assets/img/content/card-slider3.jpg" alt="alt-img">
                            </picture>
                        </div>
                        <!-- <div class="card-slider__item next-next">
                            <picture>
                                <source srcset="./_assets/img/content/card-slider4-xs.jpg" media="(max-width: 770px)">
                                <img src="./_assets/img/content/card-slider4.jpg" alt="alt-img">
                            </picture>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="button button_full button_red button_callback" data-fancybox
     data-src="#service">
    <div class="button_callback-wrap">ЗАКАЖИТЕ ОБРАТНЫЙ ЗВОНОК <span class="button__icon">

                    <svg>

                        <use xlink:href="#icon_arrow_callback" />

                    </svg>

                </span>
    </div>
</div>
<section class="section section_pt section_pb">
    <div class="container">
        <h2 class="title-2">ПОЧЕМУ АЛАРМ-МОТОРС</h2>
        <div class="advantages js-will-slide custom-slider">
            <div class="advantages__item">
                <div class="advantages__image">
                    <img src="./_assets/img/style/icons/icon_advantages2.svg" alt="">
                </div>
                <div class="advantages__descr">Удобная парковка,
                    <br />транспортная
                    <br />доступность
                </div>
            </div>
            <div class="advantages__item">
                <div class="advantages__image">
                    <img src="./_assets/img/style/icons/icon_advantages3.svg" alt="">
                </div>
                <div class="advantages__descr">Гибкая система
                    <br />лояльности
                </div>
            </div>
            <div class="advantages__item">
                <div class="advantages__image">
                    <img src="./_assets/img/style/icons/icon_advantages4.svg" alt="">
                </div>
                <div class="advantages__descr">Сертифицированные
                    <br />сотрудники
                </div>
            </div>
            <div class="advantages__item">
                <div class="advantages__image">
                    <img src="./_assets/img/style/icons/icon_advantages5.svg" alt="">
                </div>
                <div class="advantages__descr">Открытый и
                    <br />прозрачный сервис
                </div>
            </div>
            <div class="advantages__item">
                <div class="advantages__image">
                    <img src="./_assets/img/style/icons/icon_advantages6.svg" alt="">
                </div>
                <div class="advantages__descr">Наличие зоны отдыха
                    <br />с кафе, Wi-Fi,
                    <br />приветственный кофе
                </div>
            </div>
            <div class="advantages__item">
                <div class="advantages__image">
                    <img src="./_assets/img/style/icons/icon_advantages7.svg" alt="">
                </div>
                <div class="advantages__descr">Большой склад
                    <br />запчастей в наличии
                </div>
            </div>
            <div class="advantages__item">
                <div class="advantages__image">
                    <img src="./_assets/img/style/icons/icon_advantages8.svg" alt="">
                </div>
                <div class="advantages__descr">Год помощи
                    <br />на дорогах при ТО
                </div>
            </div>
            <div class="advantages__item">
                <div class="advantages__image">
                    <img src="./_assets/img/style/icons/icon_advantages10.svg" alt="">
                </div>
                <div class="advantages__descr">Ремонт в сжатые
                    <br />сроки
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section section_pt">
    <div class="container">
        <div class="contacts">
            <div class="contacts__wrap">
                <h2 class="contacts__title"><span>Контакты</span></h2>
                <div class="contacts__items">
                    <div class="contacts__item">
                        <div class="contacts__label">Телефон</div>
                        <a href="tel:+78124139242" class="contacts__value">+7 812 413-92-42</a>
                    </div>
                    <div class="contacts__item">
                        <div class="contacts__label">Адрес</div>
                        <div class="contacts__value">Санкт-Петербург,
                            <br />Выборгское шоссе, 27, к1
                        </div>
                    </div>
                    <div class="contacts__item">
                        <div class="contacts__label">Режим работы</div>
                        <div class="contacts__value">Ежедневно
                            <br />с 09:00 до 21:00
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="section">
    <div class="map-wrap">
        <div class="map" id="map"></div>
    </div>
</div>

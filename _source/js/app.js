
(function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      }
    });
  };
}(jQuery));

$(document).ready(function(){

    var screenWidth = $(window).width();
    var screenHeight = $(window).height();

    $("[data-fancybox]").fancybox({
        autoFocus: false,
        touch: false,
    });
    $(".js-masked").inputmask({"mask": "+7 (999) 999-9999"});


    var worthWrap = $(".js-worth-container.active").height();
    $(".js-worth-wrap").css({"height": worthWrap+"px"});

    $(".js-worth-tab").on("click", function(){
        $(".js-worth-tab").removeClass("active");
        $(this).addClass("active");
        var worthIdx = $(this).data("worth");
        $(".js-worth-container").removeClass("active");
        worthWrap = $(".js-worth-container[data-worth="+worthIdx+"]").height();
        $(".js-worth-wrap").css({"height": worthWrap+"px"});
        $(".js-worth-container[data-worth="+worthIdx+"]").addClass("active");
    });

    $(".js-worth-select").on("change", function(){
        var worthIdx = $(this).find("option:checked").val();
        $(".js-worth-container").removeClass("active");
        worthWrap = $(".js-worth-container[data-worth="+worthIdx+"]").height();
        $(".js-worth-wrap").css({"height": worthWrap+"px"});
        $(".js-worth-container[data-worth="+worthIdx+"]").addClass("active");
    });





    $(".js-services-check").on("change", function(){
        var servicesTotal = 0;
        $('.js-services-list').val('');
        $('.js-services-price').val('');
        $(".js-services-check:checked").each(function(){
            servicesTotal += parseInt($(this).attr('data-price'));
            $('.js-services-list').val($('.js-services-list').val()+$(this).val()+', ');
        });
        $(".js-services-total").text(servicesTotal);
        $('.js-services-price').val(servicesTotal);
    });



    $(".js-actions-slider").owlCarousel({
        items: 2,
        loop: true,
        nav: false,
        dots: false,
        margin: 20,
         responsive: {
            0: {
                items: 1,
                margin: 10,
                dots: true,
            },
            768: {
                items: 2,
                margin: 20,
                dots: false,
            },
        }
    });

    var spinWheel = $(".js-spinwheel").owlCarousel({
        items: 5,
        loop: false,
        nav: false,
        dots: false,
        margin: 20,
        smartSpeed: 1000,
        mouseDrag: false,
        touchDrag: false,
        center: true,
        startPosition: 5,
        onInitialized: function(event) {
            var stageWrap = event.target;
            var stageWidth = $(stageWrap).find(".owl-stage").width();
            $(stageWrap).find(".owl-stage").css({"width": stageWidth + 40 + "px"});
        },
        responsive: {
            0: {
                items: 1,
                margin: 35,
            },
            768: {
                items: 5,
                margin: 20,
            }
        }
    });

    $(".js-spin-btn").on("click", function(e) {
        e.preventDefault();
        if(!$(this).hasClass("disabled")) {
            var spinRandom = Math.floor(Math.random() * (49 - 42) + 42);
            spinWheel.trigger("to.owl.carousel", spinRandom);

            //если нужно писать в куки то тут
            $(this).addClass("disabled");
            var value = $(document).find('.js-spinwheel .owl-item.active.center .spinwheel__title').text();
            $('.js-spin-val').val(value);
            setTimeout(function(){
                $.fancybox.open({
                    src  : '#spin_form',
                    type : 'inline'
                });
            }, 7000)
        }
    });

    $(".js-masters-slider").owlCarousel({
        items: 3,
        loop: true,
        dots: false,
        margin: 20,
        navElement: "div",
        navClass: ["owl-control owl-prev","owl-control owl-next"],
        navText: ['<svg><use xlink:href="#icon_arrow_slider"/></svg>', '<svg><use xlink:href="#icon_arrow_slider"/></svg>'],
        nav: true,
        responsive: {
            0: {
                items: 1,
                dots: true,
                nav: false,
            },
            768: {
                items: 2,
                dots: false,
                nav: true,
            },
            1280: {
                items: 3,
            },
        }
    });
    $('.js-master-name').on("click", function(e) {
        var name = $(this).attr('data-name');
        $('.js-master-val').val(name);
    });


    $(".js-select").styler({});
    $(".js-upload").styler({});

    $(".js-upload input[type=file]").on("change", function(e){
        var filePath = document.querySelector('.js-upload input[type=file]').files[0];
        var reader = new FileReader();
        reader.onloadend = function () {
            $(".js-photos-wrap").prepend("<div class='checkprice__photos-item js-check-image'><img src='' alt=''></div>");
            $(".js-photos-wrap img[src='']").attr("src", reader.result);
          }
          if (filePath) {
            reader.readAsDataURL(filePath);
          }
    });

    $(document).on("click", ".js-check-image", function(e) {
        $(this).remove();
    });

    // Slider
    var state = {
        currentSlideIndex: 0
    };

    var slides = $('.card-slider__item');
    var slidesArray = slides.toArray();

    $('body').on('click', '.js-card-slider-next', function() {
        slidesArray.push(slidesArray[0]);
        slidesArray.shift();
        render();
    });

    $('body').on('click', '.js-card-slider-prev', function() {
        const last = slidesArray.pop();
        slidesArray.unshift(last);
        render();
    });

    var classes = ['prev', 'active', 'next', 'next-next'];

    function render() {
        $('.card-slider__item').removeClass('prev active next next-next');

        $.each(classes, function(index){
            $(slidesArray[index]).addClass(classes[index]);
        });

    }
    // end Slider

    // Функция загрузки API Яндекс.Карт по требованию (в нашем случае при скролле)
    function loadScript(url, callback) {
        var script = document.createElement("script");
        if (script.readyState) {  // IE
            script.onreadystatechange = function () {
                if (script.readyState == "loaded" ||
                    script.readyState == "complete") {
                    script.onreadystatechange = null;
                    callback();
                }
            };
        } else {  // Другие браузеры
            script.onload = function () {
                callback();
            };
        }


        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
    }

    $(window).one('scroll', function () {
        loadScript("https://api-maps.yandex.ru/2.1/?lang=ru_RU&loadByRequire=1", function () {
            ymaps.ready(function(){
                var myMap = new ymaps.Map('map', {
                    center: [60.060464, 30.306175],
                    zoom: 14.7,
                }, {
                    searchControlProvider: 'yandex#search'
                });
                myPlacemark = new ymaps.Placemark([60.060464, 30.306175], {
                }, {
                    preset: 'islands#redIcon'
                });
                myMap.geoObjects
                    .add(myPlacemark);

            });
        });
    });


    if(screenWidth >= 768) {
        $(".js-services-slider").trigger("destroy.owl.carousel");
        $(".js-services-slider").removeClass("owl-carousel");
        $(".js-will-slide").trigger("destroy.owl.carousel");
        $(".js-will-slide").removeClass("owl-carousel");

    }

    if(screenWidth < 768){
        $(".js-services-slider").addClass("owl-carousel");
        $(".js-services-slider").owlCarousel({
            items: 1,
            loop: false,
            nav: false,
            dots: true,
            margin: 15,
            onTranslated: function(event) {
                var stageWrap = event.target;
                var stageItem = $(stageWrap).find(".owl-item.active input[type=radio]");
                stageItem.click();
            }
        });
        $(".js-will-slide").addClass("owl-carousel");
        $(".js-will-slide").owlCarousel({
            items: 1,
            loop: false,
            nav: false,
            dots: true,
            margin: 0,
        });
        $(".js-kid-slider").addClass("owl-carousel");
        $(".js-kid-slider").owlCarousel({
            items: 1,
            loop: true,
            nav: true,
            dots: true,
            margin: 0,
            navElement: "div",
            navClass: ["owl-control owl-prev","owl-control owl-next"],
            navText: ['<svg><use xlink:href="#icon_arrow_slider"/></svg>', '<svg><use xlink:href="#icon_arrow_slider"/></svg>'],
        });
    };

    var servWrap = $(".js-services-container.active").height();
    $(".js-services-wrap").css({"height": servWrap+"px"});

    $(".js-services").on("change", function(){
        var servIdx = $(this).data("services");
        $(".js-services-container").removeClass("active");
        servWrap = $(".js-services-container[data-services="+servIdx+"]").height();
        $(".js-services-wrap").css({"height": servWrap+"px"});
        $(".js-services-container[data-services="+servIdx+"]").addClass("active");
    });

    /**
     * Валидация E-mail адреса
     * @param {string} emailAddress - e-mail для проверки
     * @returns {Boolean}
     */
    function isValidEmail(emailAddress) {
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        return pattern.test(emailAddress);
    }

    /**
     * Валидация всей формы
     * @param {object} form jQuery объект формы
     * @param {string} error_class класс ошибки
     * @returns {Boolean}
     */
    function isFormValidate(form, error_class) {
        var result = true,
            rq = $('.required', form).length,
            check = [
                'input[type="text"]',
                'input[type="login"]',
                'input[type="password"]',
                'input[type="number"]',
                'input[type="checkbox"]',
                'input[type="tel"]',
                'input[type="email"]',
                'input[type="file"]',
                'input[type="select"]',
                'textarea',
                'select'
            ],
            parent;

        error_class = error_class || 'has-error';

        $('.required, input, textarea, select').removeClass(error_class);

        if (rq < 1) {
            return result;
        }

        for (var i = 0; i < rq; i++) {
            parent = $('.required', form).eq(i);

            $(check.join(','), parent).each(function () {
                if (!isFieldValidate($(this), error_class)) {
                    if ($(this).attr('type') === 'checkbox' || $(this).attr('type') === 'file') {
                        $(this).closest('.jq-file').addClass(error_class);
                    } else {
                        $(this).addClass(error_class);
                    }
                    result = false;
                } else {
                    $(this).closest('.jq-file').removeClass(error_class);
                }
            });
        }
        return result;
    }

    /**
     * Проверка валидации поля
     * @param {object} field jQuery объект поля формы
     * @param {string} error_class класс ошибки
     * @returns {Boolean}
     */
    function isFieldValidate(field, error_class) {
        var result = true;
        if (field.val() == null) {
            field.val('false');
        }

        if (notNull(field) && notNull(field.attr('name')) && field.attr('name') !== '') {
            var val = (field.val() + '').trim();

            if (field.hasClass('valid_email') && !isValidEmail(val)) {
                result = false;
            } else if (field.attr('type') === 'checkbox' && !field.is(':checked')) {
                result = false;
            } else if (!notNull(val) || val === '' || val === field.data('mask')) {

                result = false;
            }
        }


        if (!result) {
            field.addClass(error_class);
        } else {
            field.removeClass(error_class);
        }

        return result;
    }

    /**
     * Проверяем значение на null и undefined
     * @param {mixed} val значение
     * @returns {Boolean}
     */
    function notNull(val) {
        return val !== null && val !== undefined;
    }

    /**
     * Изменяем все необходимые типы инпутов для мобильных устройств
     * @returns {none}
     */
    function mobilInputTypes() {
        var input_list = {
            'phone': 'tel',
            'mail': 'email',
            'email': 'email',
            'date': 'date',
            'time': 'time'
        };

        for (var i in input_list) {
            if ($('input[name="' + i + '"], input.' + i).length > 0) {
                $('input[name="' + i + '"], input.' + i).each(function () {
                    $(this).attr('type', input_list[i]);
                });
            }
        }
    }

    /**
     * Отправляем форму ajax
     * @param {object} form jQuery объект формы
     * @param {function} callback функция обратного вызова
     * @param {string} url ссылка если отправка формы не на action
     * @param {string} type mime-тип ответа
     */
    function sendFormAjax(form, callback, url, type) {
        callback = callback || function () {
        };
        var formData = new FormData(form.get(0));
        $.ajax({
            url: url || form.attr('action') || '/send',
            dataType: type || 'json',
            contentType: false, // важно - убираем форматирование данных по умолчанию
            processData: false, // важно - убираем преобразование строк по умолчанию
            type: 'POST',
            data: formData,
        }).then(function (data) {
            callback(data);
        }).fail(function (request, status, error) {
            callback({
                'type': 'error',
                'class': 'danger',
                'text': error,
                'request': request
            });
        });
    }

    /**
     * Обработка отправки формы
     * @param {object} form jQuery объект формы
     * @param {object} data данные полученные от сервера
     */
    function formSendResult(form, data) {
        if (data.status === 'success') {
            form.html(data.content);
        } else {
            $('.form__error-block', form.parent()).remove();
            form.before('<div class="form__error-block"></div>');
            $('.form__error-block', form.parent()).append(data.content);
        }
    }

    $('body').on('submit', 'form', function (e) {
        var t = $(this);
        if (!isFormValidate(t)) {
            e.preventDefault();
            e.stopPropagation();
            $('input:text.has-error', t).first().focus();

        } else if (t.hasClass('js-ajax-form')) {
            e.preventDefault();
            e.stopPropagation();

            t.addClass('load');

            sendFormAjax(t, function (data) {
                formSendResult(t, data);
                t.removeClass('load');
            });
        }
    });

});


$(window).resize(function() {
    var screenWidth = $(window).width();
    var screenHeight = $(window).height();
});
